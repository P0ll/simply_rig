import bpy
from bpy.types import Panel, Operator, PropertyGroup, PoseBone, Scene, Menu
from bpy.props import StringProperty, BoolProperty, IntProperty, FloatProperty, FloatVectorProperty, EnumProperty, PointerProperty
from bpy.utils import register_class, unregister_class

#-------------------------- ID
 
rig_id = "simply_rig"
rig_type = "biped"

#-------------------------- Tabs 

ui_list = ['Item', 'Simply rig UI'] # space avaibles to draw the panel
ui_location = ui_list[1] # this value chooses the location to run the panel

#-------------------------- Icon style

submenus = ["RIGHTARROW", "DOWNARROW_HLT"]
advance_settings_tabs = ["MODIFIER_OFF", "MODIFIER_ON"]
eyes_icon = ["HIDE_OFF", "HIDE_ON"]

#-------------------------- Bones Groups
                            # write here the naming convention for your controls
                            # important! Do not change the order of the controller names, to do so will break the IK/FK snap system

arm_left = ['properties_arm_L', 'hand_FK_L', 'forearm_FK_L' , 'upperarm_FK_L', 'hand_IK_L', 'forearm_IK_L' , 'upperarm_IK_L', 'elbow_pole_L', 'arm_FK_L', 'arm_strech_length_L', 'hand_IK_pivot_L', 'arm_toon_3_L', 'arm_toon_2_L', 'arm_toon_1_L', 'shoulder_L']
arm_right = ['properties_arm_R', 'hand_FK_R', 'forearm_FK_R' , 'upperarm_FK_R', 'hand_IK_R', 'forearm_IK_R' , 'upperarm_IK_R', 'elbow_pole_R', 'arm_FK_R', 'arm_strech_length_R', 'hand_IK_pivot_R', 'arm_toon_3_R', 'arm_toon_2_R', 'arm_toon_1_R', 'shoulder_R']
leg_left = ['properties_leg_L', 'foot_FK_L', 'shin_FK_L' , 'thigh_FK_L', 'foot_IK_L', 'shin_IK_L' , 'thigh_IK_L', 'knee_pole_L', 'leg_FK_L', 'leg_strech_length_L', 'foot_IK_pivot_L', 'ankle_roll_L', 'toe_L', 'leg_toon_3_L', 'leg_toon_2_L', 'leg_toon_1_L']
leg_right = ['properties_leg_R', 'foot_FK_R', 'shin_FK_R' , 'thigh_FK_R', 'foot_IK_R', 'shin_IK_R' , 'thigh_IK_R', 'knee_pole_R', 'leg_FK_R', 'leg_strech_length_R', 'foot_IK_pivot_R', 'ankle_roll_R', 'toe_R', 'leg_toon_3_R', 'leg_toon_2_R', 'leg_toon_1_R']
hand_properties_left = ['properties_fh_thumb_L', 'properties_fh_index_L', 'properties_fh_middle_L', 'properties_fh_ring_L', 'properties_fh_pinky_L', 'fingers_IK_L', 'fingers_L']
hand_properties_right = ['properties_fh_thumb_R', 'properties_fh_index_R', 'properties_fh_middle_R', 'properties_fh_ring_R', 'properties_fh_pinky_R', 'fingers_IK_R', 'fingers_R']

hand_fingers_thumb_left = ['fh_thumb_general_L', 'fh_thumb_1_L', 'fh_thumb_2_L', 'fh_thumb_3_L', 'fh_thumb_IK_L', 'fh_thumb_deformer_L']
hand_fingers_thumb_right = ['fh_thumb_general_R', 'fh_thumb_1_R', 'fh_thumb_2_R', 'fh_thumb_3_R', 'fh_thumb_IK_R', 'fh_thumb_deformer_R']
hand_fingers_index_left = ['fh_index_general_L', 'fh_index_1_L', 'fh_index_2_L', 'fh_index_3_L', 'fh_index_4_L', 'fh_index_IK_L', 'fh_index_deformer_L']
hand_fingers_index_right = ['fh_index_general_R', 'fh_index_1_R', 'fh_index_2_R', 'fh_index_3_R', 'fh_index_4_R', 'fh_index_IK_R', 'fh_index_deformer_R']
hand_fingers_middle_left = ['fh_middle_general_L', 'fh_middle_1_L', 'fh_middle_2_L', 'fh_middle_3_L', 'fh_middle_4_L', 'fh_middle_IK_L', 'fh_middle_deformer_L']
hand_fingers_middle_right = ['fh_middle_general_R', 'fh_middle_1_R', 'fh_middle_2_R', 'fh_middle_3_R', 'fh_middle_4_R', 'fh_middle_IK_R', 'fh_middle_deformer_R']
hand_fingers_ring_left = ['fh_ring_general_L', 'fh_ring_1_L', 'fh_ring_2_L', 'fh_ring_3_L', 'fh_ring_4_L', 'fh_ring_IK_L', 'fh_ring_deformer_L']
hand_fingers_ring_right = ['fh_ring_general_R', 'fh_ring_1_R', 'fh_ring_2_R', 'fh_ring_3_R', 'fh_ring_4_R', 'fh_ring_IK_R', 'fh_ring_deformer_R']
hand_fingers_pinky_left = ['fh_pinky_general_L', 'fh_pinky_1_L', 'fh_pinky_2_L', 'fh_pinky_3_L', 'fh_pinky_4_L', 'fh_pinky_IK_L', 'fh_pinky_deformer_L']
hand_fingers_pinky_right = ['fh_pinky_general_R', 'fh_pinky_1_R', 'fh_pinky_2_R', 'fh_pinky_3_R', 'fh_pinky_4_R', 'fh_pinky_IK_R', 'fh_pinky_deformer_R']

head = ['head_rot', 'head', 'head_toon', 'neck_rot']

torso = ['chest_IK', 'chest_FK', 'spine_FK_1', 'spine_FK_2', 'spine_FK_inv_1', 'spine_FK_inv_2', 'spine_toon', 'hip_IK', 'hip_FK']

#-------------------------- Flip Groups
                            # important no change the order for the (1º)left and (2º)right groups
                                                        
icon_flip = ['EVENT_L','EVENT_R']
arm_flip = [arm_left[0], arm_right[0]]
leg_flip = [leg_left[0], leg_right[0]]
hand_flip = [hand_properties_left[0], hand_properties_right[0], hand_properties_left[1], hand_properties_right[1], hand_properties_left[2], hand_properties_right[2], hand_properties_left[3], hand_properties_right[3], hand_properties_left[4], hand_properties_right[4], hand_properties_left[5], hand_properties_right[5]]
arm_names_flip = [arm_left, arm_right]
leg_names_flip = [leg_left, leg_right]
hand_names_flip = [hand_properties_left, hand_properties_right, hand_fingers_thumb_left, hand_fingers_thumb_right, hand_fingers_index_left, hand_fingers_index_right, hand_fingers_middle_left, hand_fingers_middle_right, hand_fingers_ring_left, hand_fingers_ring_right, hand_fingers_pinky_left, hand_fingers_pinky_right]

#-------------------------- Misc Groups

fk_arm_left = [arm_left[1], arm_left[2], arm_left[3], arm_left[8]]
fk_arm_right = [arm_right[1], arm_right[2], arm_right[3], arm_right[8]]
ik_arm_left = [arm_left[4], arm_left[5], arm_left[6], arm_left[7], arm_left[9], arm_left[10]]
ik_arm_right = [arm_right[4], arm_right[5], arm_right[6], arm_right[7], arm_right[9], arm_right[10]]

fk_leg_left = [leg_left[1], leg_left[2], leg_left[3], leg_left[8], leg_left[12]]
fk_leg_right = [leg_right[1], leg_right[2], leg_right[3], leg_right[8], leg_right[12]]
ik_leg_left = [leg_left[4], leg_left[5], leg_left[6], leg_left[7], leg_left[9], leg_left[10], leg_left[11]]
ik_leg_right = [leg_right[4], leg_right[5], leg_right[6], leg_right[7], leg_right[9], leg_right[10], leg_right[11]]

euler_channels = ['WXYZ','XYZ','XZY','YXZ','YZX','ZXY','ZYX']

#-------------------------- Panel Properties
    
class MenuProp(PropertyGroup):
    
    settings_keyframes_props: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    settings_keyframes_ctrls: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    
    menu_pose_tools: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    menu_properties_all: BoolProperty(name="", description="", default = False, override={'LIBRARY_OVERRIDABLE'})
    menu_settings_head: BoolProperty(name="", description="", default = False, override={'LIBRARY_OVERRIDABLE'})
    menu_settings_arm: BoolProperty(name="", description="", default = False, override={'LIBRARY_OVERRIDABLE'})
    menu_settings_hand: BoolProperty(name="", description="", default = False, override={'LIBRARY_OVERRIDABLE'})
    menu_settings_torso: BoolProperty(name="", description="", default = False, override={'LIBRARY_OVERRIDABLE'})
    menu_settings_leg: BoolProperty(name="", description="", default = False, override={'LIBRARY_OVERRIDABLE'})
    
    menu_axis_arm: BoolProperty(name="", description="", default = False, override={'LIBRARY_OVERRIDABLE'})
    menu_axis_hand: BoolProperty(name="", description="", default = False, override={'LIBRARY_OVERRIDABLE'})
    menu_axis_leg: BoolProperty(name="", description="", default = False, override={'LIBRARY_OVERRIDABLE'})
    menu_pole_arm: BoolProperty(name="", description="", default = False, override={'LIBRARY_OVERRIDABLE'})
    menu_pole_leg: BoolProperty(name="", description="", default = False, override={'LIBRARY_OVERRIDABLE'})
    
    advance_settings_arms: BoolProperty(name="", description="", default = False, override={'LIBRARY_OVERRIDABLE'})
    advance_settings_legs: BoolProperty(name="", description="", default = False, override={'LIBRARY_OVERRIDABLE'})
    advance_settings_layers: BoolProperty(name="", description="", default = False, override={'LIBRARY_OVERRIDABLE'})
    advance_settings_rig: BoolProperty(name="", description="", default = False, override={'LIBRARY_OVERRIDABLE'})

#-------------------------- Functions

def is_selected(names):
    try:
        for name in names:
            if bpy.context.active_pose_bone.name == name:
                return True
        for bone in bpy.context.selected_pose_bones:
            for name in names:
                if bone.name == name:
                    return True
    except AttributeError:
        pass
    return False

#-------------------------- Operators

class OBJECT_OT_SelectBone(Operator):
    bl_idname = "object.select_bone"
    bl_label = "Select given bone"
    bone_name: StringProperty(name="bone_name") 
    
    def execute(self, context):
        allBones=bpy.context.object.pose.bones #get all bones
        
        for thisbone in allBones:
            thisbone.bone.select=False #deselect all bones
                
        allBones[self.bone_name].bone.select=True #select the one we want
        return {'FINISHED'}

groupspick = [fk_arm_left, fk_arm_right, fk_leg_left, fk_leg_right, ik_arm_left, ik_arm_right, ik_leg_left, ik_leg_right]
groupsprop = [arm_left[0], arm_right[0], leg_left[0], leg_right[0], "keyframe_arms", "keyframe_arms", "keyframe_legs", "keyframe_legs"]
vsbctr = ["visibility_IKFK_arm_L", "visibility_IKFK_arm_R", "visibility_IKFK_leg_L", "visibility_IKFK_leg_R"]
snaplayers = [2, 3, 18, 19]

class OBJECT_OT_Snap(Operator):
    
    """normalize coordinates of the IK & FK bones"""
    
    bl_idname = "object.snap" 
    bl_label = "snap"
    s_name: IntProperty(name = "", description="", default = 0, min = 0, max = 100)
    
    def execute(self, context):
            
        bpy.ops.pose.select_all(action='DESELECT')
        bpy.context.object.pose.bones["properties_panel"][vsbctr[self.s_name]] = 1
        bpy.context.object.data.layers[snaplayers[self.s_name]] = True

        if (bpy.context.object.pose.bones[groupsprop[self.s_name]].IK == 0):
            
            if self.s_name>1:
                bpy.ops.object.select_pattern(pattern= groupspick[self.s_name+4][6])
                
            bpy.ops.object.select_pattern(pattern= groupspick[self.s_name+4][5])
            bpy.ops.pose.transforms_clear() 
            for iks in groupspick[self.s_name+4]:
                bpy.ops.object.select_pattern(pattern= iks)
            
            bpy.ops.anim.keyframe_insert_menu(type='BUILTIN_KSI_VisualLocRotScale')
            bpy.context.scene.frame_set(bpy.context.scene.frame_current)
            
            for fks in groupspick[self.s_name]:
                bpy.ops.object.select_pattern(pattern= fks)
            
            bpy.ops.anim.keyframe_insert_menu(type='LocRotScale')
            bpy.context.scene.frame_set(bpy.context.scene.frame_current)
            
            bpy.context.object.pose.bones[groupsprop[self.s_name]].IK = 1
            
        else:
            bpy.ops.object.select_pattern(pattern= groupspick[self.s_name][3])
            bpy.ops.pose.transforms_clear()
            for fks in groupspick[self.s_name]:
                bpy.ops.object.select_pattern(pattern= fks)
            
            bpy.ops.anim.keyframe_insert_menu(type='BUILTIN_KSI_VisualLocRotScale')
            bpy.context.scene.frame_set(bpy.context.scene.frame_current)
            
            for iks in groupspick[self.s_name+4]:
                bpy.ops.object.select_pattern(pattern= iks)
            
            bpy.ops.anim.keyframe_insert_menu(type='LocRotScale')
            bpy.context.scene.frame_set(bpy.context.scene.frame_current)
            
            bpy.context.object.pose.bones[groupsprop[self.s_name]].IK = 0

        if (bpy.context.object.pose.bones['properties_panel'][groupsprop[self.s_name+4]] == 1):
            bpy.ops.pose.select_all(action='DESELECT')
            bpy.ops.object.select_pattern(pattern= groupsprop[self.s_name])
            bpy.ops.anim.keyframe_insert_menu(type='WholeCharacterSelected')
            bpy.context.scene.frame_set(bpy.context.scene.frame_current)
            
        else:
            bpy.ops.anim.keyframe_delete_v3d()
            
        if self.s_name == (0 or 2):   
            if (bpy.context.object.pose.bones[groupspick[self.s_name+4][1]].rotation_euler[2] >0):
                
                bpy.ops.pose.select_all(action='DESELECT')
                bpy.ops.object.select_pattern(pattern= groupspick[self.s_name+4][1])
                bpy.ops.pose.rot_clear()
                bpy.context.scene.frame_set(bpy.context.scene.frame_current)
                
        else:
            if (bpy.context.object.pose.bones[groupspick[self.s_name+4][1]].rotation_euler[2] <0):
                
                bpy.ops.pose.select_all(action='DESELECT')
                bpy.ops.object.select_pattern(pattern= groupspick[self.s_name+4][1])
                bpy.ops.pose.rot_clear()
                bpy.context.scene.frame_set(bpy.context.scene.frame_current)
                        
        bpy.ops.pose.select_all(action='DESELECT')
        bpy.context.object.pose.bones["properties_panel"][vsbctr[self.s_name]] = 0
        
        if (bpy.context.object.pose.bones[groupsprop[self.s_name]].IK == 0):
            bpy.ops.object.select_pattern(pattern= groupspick[self.s_name][0])
        else:
            bpy.ops.object.select_pattern(pattern= groupspick[self.s_name+4][0])   
                        
        return {'FINISHED'}

class OBJECT_OT_SnapGeneral(Operator):
    
    """normalize coordinates of the IK & FK bones of selected groups"""
    
    bl_idname = "object.general_snap" 
    bl_label = "general snap"
    
    def execute(self, context):
        
        if is_selected(arm_names_flip[0]):
            bpy.ops.object.snap(s_name=0)
            
        if is_selected(arm_names_flip[1]):
            bpy.ops.object.snap(s_name=1)
            
        if is_selected(leg_names_flip[0]):    
            bpy.ops.object.snap(s_name=2)
            
        if is_selected(leg_names_flip[1]):
            bpy.ops.object.snap(s_name=3)
        
        return {'FINISHED'}
    
class OBJECT_OT_PickGroup(Operator):
    
    """pick all parts"""
    
    bl_idname = "object.pick_group" 
    bl_label = "pick group"
    p_name: IntProperty(name = "", description="", default = 0, min = 0, max = 100)
    
    def invoke(self, context, event):
        
        if event.shift == False:
            bpy.ops.pose.select_all(action='DESELECT')
            
        if (bpy.context.object.pose.bones[groupsprop[self.p_name]].IK == False):
            for pflr in groupspick[self.p_name]:
                bpy.ops.object.select_pattern(pattern= pflr)
        else:
            for pilr in groupspick[self.p_name+4]:
                bpy.ops.object.select_pattern(pattern= pilr)
        
        return {'FINISHED'}
           
groupsreset = [arm_left, arm_right, leg_left, leg_right, hand_fingers_thumb_left + hand_fingers_index_left + hand_fingers_middle_left + hand_fingers_pinky_left + hand_properties_left, hand_fingers_thumb_right + hand_fingers_index_right + hand_fingers_middle_right + hand_fingers_pinky_right + hand_properties_right, hand_fingers_ring_left, hand_fingers_ring_right, head, torso]

class OBJECT_OT_ResetGroup(Operator):
    
    """reset coordinates"""
    
    bl_idname = "object.reset_group" 
    bl_label = "reset group"
    g_name: IntProperty(name = "", description="", default = 0, min = 0, max = 100)
    
    def execute(self, context):
            
        for rstg in groupsreset[self.g_name]:
            bpy.context.object.pose.bones[rstg].rotation_quaternion = (1.0, 0.0, 0.0, 0.0)
            bpy.context.object.pose.bones[rstg].rotation_euler = (0.0, 0.0, 0.0)
            bpy.context.object.pose.bones[rstg].location = (0.0, 0.0, 0.0)
            bpy.context.object.pose.bones[rstg].scale = (1.0, 1.0, 1.0)
            
            if self.g_name>3 and self.g_name<6 and bpy.context.object.pose.bones['properties_panel']["rig_hand_fingers"] == 5:
                
                for hfrl in groupsreset[self.g_name+2]:
                    bpy.context.object.pose.bones[hfrl].rotation_quaternion = (1.0, 0.0, 0.0, 0.0)
                    bpy.context.object.pose.bones[hfrl].rotation_euler = (0.0, 0.0, 0.0)
                    bpy.context.object.pose.bones[hfrl].location = (0.0, 0.0, 0.0)
                    bpy.context.object.pose.bones[hfrl].scale = (1.0, 1.0, 1.0)  
    
        return {'FINISHED'}

allkeyprops = ['properties_head', 'properties_torso', arm_left[0], arm_right[0], leg_left[0], leg_right[0], hand_properties_left[0], hand_properties_right[0], hand_properties_left[1], hand_properties_right[1], hand_properties_left[2], hand_properties_right[2], hand_properties_left[3], hand_properties_right[3], hand_properties_left[4], hand_properties_right[4], hand_properties_left[5], hand_properties_right[5]]
layersall = [1, 2, 3, 4, 5, 16, 17, 18, 19]
boolsall = ["keyframe_arms", "keyframe_legs", "layer_toon_arm_L", "layer_toon_arm_R", "layer_toon_leg_L", "layer_toon_leg_R", "virtual_pivot_torso", "virtual_pivot_master"]

class OBJECT_OT_KeyframeProperties(Operator):
    
    """Insert keyframe on all properties"""
    
    bl_idname = "object.keyframe_properties" 
    bl_label = "keyframe properties"
    
    def execute(self, context):
        
        bpy.context.object.data.layers[0] = True
        bpy.ops.pose.select_all(action='DESELECT')
        for akp in allkeyprops:
            bpy.ops.object.select_pattern(pattern= akp)
        if bpy.context.object.pose.bones['properties_panel'].menu_prop.settings_keyframes_props == True:
            bpy.ops.anim.keyframe_insert_menu(type='WholeCharacterSelected')
            bpy.context.scene.frame_set(bpy.context.scene.frame_current)
        bpy.context.scene.tool_settings.use_keyframe_insert_auto = True
        for bltr in boolsall:
            bpy.context.object.pose.bones["properties_panel"][bltr] = True
        for lyrl in layersall:
             bpy.context.object.data.layers[lyrl] = True
        if bpy.context.object.pose.bones['properties_panel'].menu_prop.settings_keyframes_ctrls == True:
            bpy.ops.pose.select_all(action='INVERT')
            bpy.ops.anim.keyframe_insert_menu(type='LocRotScale')
            bpy.context.scene.frame_set(bpy.context.scene.frame_current)
        bpy.ops.pose.select_all(action='DESELECT')
        bpy.context.object.data.layers[0] = False
        
        return {'FINISHED'}

#-------------------------- Menus

class OBJECT_MT_ResetSettings(Menu):
    bl_label = "Select"
    bl_idname = "OBJECT_MT_ResetSettings"

    def draw(self, context):
        layout = self.layout
        layout.prop(bpy.context.active_object.pose.bones['properties_panel'].menu_prop,'settings_keyframes_ctrls', text=" Controls")
        layout.prop(bpy.context.active_object.pose.bones['properties_panel'].menu_prop,'settings_keyframes_props', text=" Properties")
        
#-------------------------- Properties UI             

class PANEL_PT_RigProperties(Panel):
    
    """Draw panels for rig controls"""
    
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = ui_location
    bl_label = ""
    
    def draw_header(self, context):
        self.layout.label(text=bpy.data.objects[bpy.context.active_object.name].pose.bones["properties_panel"]["character_name"] + " Properties", icon="PREFERENCES")

    @classmethod
    def poll(self, context):
        if context.mode != 'POSE':
            return False

        try:
           ob = context.active_object
           return (context.active_object.data.get("rig_id") == rig_id) and (context.active_object.data.get("rig_type") == rig_type)
        except AttributeError:
           return 0
       
    def draw(self, context):
        IK_double_values = bpy.context.scene.menu_propscn.IK_double_values
        flip = bpy.context.scene.menu_propscn.flip
        
        layout = self.layout
        
        col_1 = layout.column(align = 0)
        col_1a = col_1.row(align = 1)
        
        col = layout.column()
        
        ctrl_and_props = ["nothing", "all controls", "all properties", "all controls and properties"]
        
        col_1a.menu(OBJECT_MT_ResetSettings.bl_idname,text="", icon="SETTINGS")
        col_1a.operator('object.keyframe_properties',text="Insert keyframes on "+ ctrl_and_props[(bpy.context.object.pose.bones['properties_panel'].menu_prop.settings_keyframes_ctrls + (bpy.context.object.pose.bones['properties_panel'].menu_prop.settings_keyframes_props*2))], icon="KEYTYPE_KEYFRAME_VEC")
                
        if bpy.context.scene.menu_propscn.pose_tools == True:
            
            col_1b = col_1.row(align = 1)
            col_1b.separator()
            col_1b = col_1.row(align = 1)
            
            row = col_1.row()

            row.alignment = 'LEFT'
            row.prop(bpy.context.active_object.pose.bones['properties_panel'].menu_prop, 'menu_pose_tools', text="Pose Tools", toggle=True, icon= submenus[1 - bpy.context.object.pose.bones['properties_panel'].menu_prop["menu_pose_tools"]], emboss = False)            
        
            if (bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_pose_tools == False):
                
                box = layout.row()
                box = col_1.box()
                row = box.row()

                row.operator("pose.copy", icon='COPYDOWN')
                row.operator("pose.paste", icon='PASTEDOWN').flipped = False
                row.operator("pose.paste", icon='PASTEFLIPDOWN', text="Paste Pose Flipped").flipped = True
                row = box.row()

                row.scale_x=1.8
                row.prop(bpy.context.object.pose, "use_mirror_x", toggle = True)
                row.scale_x=0.5
                row.prop(bpy.context.object.pose, "use_mirror_relative", text='Offset')
                col = layout.column()
        
        col_1c = col_1.row(align = 1)
        col_1c.separator()
        col_1c = col_1.row(align = 1)
        
        col_1c.prop(bpy.context.active_object.pose.bones['properties_panel'].menu_prop,'menu_properties_all', text="Show by groups", icon="PRESET", toggle=True)
        col_1c.prop(bpy.context.active_object.pose.bones['properties_panel'].menu_prop,'menu_properties_all', text="Show by selection", icon="RESTRICT_SELECT_OFF", toggle=True, invert_checkbox = True)
        
        if (bpy.context.object.pose.bones['properties_panel'].menu_prop["menu_properties_all"] == True):
            
            box = layout.row()
            col = box.column()
            box = col.box()
            row = box.row()
            row.prop(bpy.context.active_object.pose.bones['properties_panel'].menu_prop,'menu_settings_head', text=" Head")
            row.prop(bpy.context.active_object.pose.bones['properties_panel'].menu_prop,'menu_settings_arm', text=" Arms")
            row.prop(bpy.context.active_object.pose.bones['properties_panel'].menu_prop,'menu_settings_leg', text=" Legs")
            row = box.row()
            row.prop(bpy.context.active_object.pose.bones['properties_panel'].menu_prop,'menu_settings_torso', text=" Torso")
            row.prop(bpy.context.active_object.pose.bones['properties_panel'].menu_prop,'menu_settings_hand', text=" Fingers")
            row.label(text="")
            col = layout.column()
                            
#-------------------------- Properties UI Head
        
        if (bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_settings_head == True and bpy.context.object.pose.bones['properties_panel'].menu_prop["menu_properties_all"] == True) or (is_selected(head) and bpy.context.object.pose.bones['properties_panel'].menu_prop["menu_properties_all"] == False):
            
            row = col.row()
            
            row.alignment = 'CENTER'
            row.label(text="Head settings")
            
            box = layout.row()
            col = box.column()
            box = col.box()
            
            row = box.row() 
            
            row.operator("object.reset_group",text="Reset controls", icon="LOOP_BACK").g_name = 8
            row = box.row()
            row.prop(bpy.context.active_object.pose.bones['properties_head'],'no_rot', text=" Follow body", invert_checkbox=True)

            row = box.row()

            row.label(text="Eyes space")
            
            row = box.row()
            
            spaces = ['Master','Head','Root']
            
            row.prop(bpy.context.active_object.pose.bones['properties_head'],'pole_space', text= spaces[0 + bpy.context.object.pose.bones['properties_head']["pole_space"]])

            row = box.row()  
            row.prop(bpy.context.active_object.pose.bones['properties_head'],'strech', text="Eyelids follow", slider= True)

#-------------------------- Properties UI Arms
            
        if (bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_settings_arm == True and bpy.context.object.pose.bones['properties_panel'].menu_prop["menu_properties_all"] == True) or (is_selected(arm_names_flip[0] + arm_names_flip[1]) and bpy.context.object.pose.bones['properties_panel'].menu_prop["menu_properties_all"] == False):
            
            row = col.row()
            
            row.alignment = 'CENTER'
            row.label(text="Arms settings")
            
            box = layout.row()
            col = box.column()
            box = col.box()
            
            col_row = box.row()
            
            if is_selected(arm_names_flip[0+flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:
                
                col_1 = col_row.column(align = 0)
                
                col_1a = col_1.row(align = 0)
                col_1a.label(text="")
                col_1a.operator("object.pick_group",text="", icon=icon_flip[0+flip], emboss = False).p_name = 0+flip
                col_1a.label(text="")
                col_1a = col_1.row(align = 0)
                col_1a.separator()
                col_1a = col_1.row(align = 0)
                col_1a.operator("object.reset_group",text="Reset controls", icon="LOOP_BACK").g_name = 0+flip
                col_1a = col_1.row(align = 0)
                col_1a.separator()
                col_1a = col_1.row(align = 0)
                col_1a.prop(bpy.context.active_object.pose.bones[arm_flip[0+flip]],'no_rot', text="Follow body", invert_checkbox=True)
                
            if is_selected(arm_names_flip[1-flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:
                
                col_2 = col_row.column(align = 0)
                
                col_2a = col_2.row(align = 0)
                col_2a.label(text="")
                col_2a.operator("object.pick_group",text="", icon=icon_flip[1-flip], emboss = False).p_name = 1-flip
                col_2a.label(text="")
                col_2a = col_2.row(align = 0)
                col_2a.separator()
                col_2a = col_2.row(align = 0)
                col_2a.operator("object.reset_group",text="Reset controls", icon="LOOP_BACK").g_name = 1-flip
                col_2a = col_2.row(align = 0)
                col_2a.separator()
                col_2a = col_2.row(align = 0)
                col_2a.prop(bpy.context.active_object.pose.bones[arm_flip[1-flip]],'no_rot', text="Follow body", invert_checkbox=True)
            
            col_row = box.row()
            
            if is_selected(arm_names_flip[0+flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:
                
                col_1 = col_row.column(align = 1)
                
                col_1a = col_1.row(align = 1)
                if (bpy.context.object.pose.bones[arm_flip[0+flip]].IK <1) & (bpy.context.object.pose.bones[arm_flip[0+flip]].IK >0):
                    col_1a.label(text="need IK 0.0 or 1.0", icon="UNLINKED")
                else:
                    col_1a.operator("object.snap",text="Snap & swicht", icon="FILE_REFRESH").s_name = 0+flip
                col_1a = col_1.row(align = 1)
                col_1a.separator()    
                col_1a = col_1.row(align = 1)
                col_1a.operator("object.select_bone",text="", icon="VIS_SEL_11").bone_name=arm_flip[0+flip]
                col_1a.prop(bpy.context.active_object.pose.bones[arm_flip[0+flip]],'IK', toggle=True, text="IK")
                if IK_double_values == True:
                    col_1a.prop(bpy.context.active_object.pose.bones[arm_flip[0+flip]],'IK', toggle=True, text="FK", invert_checkbox=True)
                col_1a = col_1.row(align = 1)
                col_1a.prop(bpy.context.active_object.pose.bones[arm_flip[0+flip]],'strech', slider= True, text="Strech", emboss = (bpy.context.object.pose.bones[arm_flip[0+flip]].IK == True))
            
            if is_selected(arm_names_flip[1-flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:
                
                col_2 = col_row.column(align = 1)
                
                col_2a = col_2.row(align = 1)
                if (bpy.context.object.pose.bones[arm_flip[1-flip]].IK <1) & (bpy.context.object.pose.bones[arm_flip[1-flip]].IK >0):
                    col_2a.label(text="need IK 0.0 or 1.0", icon="UNLINKED") 
                else:
                    col_2a.operator("object.snap",text="Snap & swicht", icon="FILE_REFRESH").s_name = 1-flip
                col_2a = col_2.row(align = 1)
                col_2a.separator()    
                col_2a = col_2.row(align = 1)
                col_2a.operator("object.select_bone",text="", icon="VIS_SEL_11").bone_name=arm_flip[1-flip]
                col_2a.prop(bpy.context.active_object.pose.bones[arm_flip[1-flip]],'IK', toggle=True, text="IK")
                if IK_double_values == True:
                    col_2a.prop(bpy.context.active_object.pose.bones[arm_flip[1-flip]],'IK', toggle=True, text="FK", invert_checkbox=True)
                col_2a = col_2.row(align = 1)
                col_2a.prop(bpy.context.active_object.pose.bones[arm_flip[1-flip]],'strech', slider= True, text="Strech", emboss = (bpy.context.object.pose.bones[arm_flip[1-flip]].IK == True)) 
              
            row = box.row()
            
            row.alignment = 'LEFT'

            row.prop(bpy.context.active_object.pose.bones['properties_panel'].menu_prop,'menu_axis_arm', text="Rotation axis order", toggle=True, icon= submenus[1 - bpy.context.active_object.pose.bones['properties_panel'].menu_prop.menu_axis_arm], emboss = False)

            row = box.row()
            
            axis_grp = ["Upperarm", "Forearm", "Hand"]
            axis_grp2 = ['["axis_order_chnl_3"]', '["axis_order_chnl_2"]', '["axis_order_chnl_1"]']
            axis_grp3 = ["axis_order_chnl_3", "axis_order_chnl_2", "axis_order_chnl_1"]
            
            drw_ax = [0, 1, 2]

            if (bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_axis_arm == False):
                
                for axgp in drw_ax: 
                    row.label(text=axis_grp[axgp])
                    
                    row = box.row()
                    
                    if is_selected(arm_names_flip[0+flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:
                               
                        row.prop(bpy.context.active_object.pose.bones[arm_flip[0+flip]],axis_grp2[axgp], text= euler_channels[0 + bpy.context.object.pose.bones[arm_flip[0+flip]][axis_grp3[axgp]]])
                        
                    if is_selected(arm_names_flip[1-flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:    
                    
                        row.prop(bpy.context.active_object.pose.bones[arm_flip[1-flip]],axis_grp2[axgp], text= euler_channels[0 + bpy.context.object.pose.bones[arm_flip[1-flip]][axis_grp3[axgp]]])
                    
                    row = box.row()
                            
            row.alignment = 'LEFT'
            row.prop(bpy.context.active_object.pose.bones['properties_panel'].menu_prop,'menu_pole_arm', text="Pole settings", toggle=True, icon= submenus[1 - bpy.context.active_object.pose.bones['properties_panel'].menu_prop.menu_pole_arm], emboss = False)
                
            row = box.row()
                
            if (bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_pole_arm == False):
                
                if is_selected(arm_names_flip[0+flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:
                    
                    row.prop(bpy.context.active_object.pose.bones[arm_flip[0+flip]],'pole_lock', text=" Pin elbow to pole")
                        
                if is_selected(arm_names_flip[1-flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:
                    
                    row.prop(bpy.context.active_object.pose.bones[arm_flip[1-flip]],'pole_lock', text=" Pin elbow to pole")
                
                row = box.row()  
                
                row.label(text="Space follow") 
                
                row = box.row()
                
                spaces = ['Master','Hand','Root']
                
                if is_selected(arm_names_flip[0+flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:

                    row.prop(bpy.context.active_object.pose.bones[arm_flip[0+flip]],'pole_space', text= spaces[0 + bpy.context.object.pose.bones[arm_flip[0+flip]]["pole_space"]])

                if is_selected(arm_names_flip[1-flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:    

                    row.prop(bpy.context.active_object.pose.bones[arm_flip[1-flip]],'pole_space', text= spaces[0 + bpy.context.object.pose.bones[arm_flip[1-flip]]["pole_space"]])

                row = box.row()

            row.alignment = 'RIGHT'
            row.prop(bpy.context.active_object.pose.bones['properties_panel'].menu_prop,'advance_settings_arms', text= "Advance settings", icon= advance_settings_tabs[0 + bpy.context.object.pose.bones['properties_panel'].menu_prop.advance_settings_arms], emboss = False)
            
            if (bpy.context.object.pose.bones['properties_panel'].menu_prop.advance_settings_arms == True):
                
                col = box.column()
                box = col.box()
            
                row = box.row()
                
                row.alignment = 'RIGHT'
                row.prop(bpy.context.active_object.pose.bones['properties_panel'],'keyframe_arms', text=' Autokeyframe')
                
                row = box.row()

                row.alignment = 'CENTER'
                row.label(text="Experimental", icon="EXPERIMENTAL")
                row = box.row()
                
                if is_selected(arm_names_flip[0+flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:
                    
                    row.prop(bpy.context.active_object.pose.bones[arm_flip[0+flip]],'no_pole', toggle=True, text="Pole", icon= eyes_icon[0 + bpy.context.object.pose.bones[arm_flip[0+flip]]["no_pole"]], invert_checkbox=True)
                
                if is_selected(arm_names_flip[1-flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:
                    
                    row.prop(bpy.context.active_object.pose.bones[arm_flip[1-flip]],'no_pole', toggle=True, text="Pole", icon= eyes_icon[0 +  bpy.context.object.pose.bones[arm_flip[1-flip]]["no_pole"]], invert_checkbox=True)

            col = layout.column()
            
#-------------------------- Properties UI Hands Fingers               
        
        if (bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_settings_hand == True and bpy.context.object.pose.bones['properties_panel'].menu_prop["menu_properties_all"] == True) or (is_selected(hand_names_flip[0] + hand_names_flip[1] + hand_names_flip[2] + hand_names_flip[3] + hand_names_flip[4] + hand_names_flip[5] + hand_names_flip[6] + hand_names_flip[7] + hand_names_flip[8] + hand_names_flip[9] + hand_names_flip[10] + hand_names_flip[11]) and bpy.context.object.pose.bones['properties_panel'].menu_prop["menu_properties_all"] == False):
            
            row = col.row()
            
            row.alignment = 'CENTER'
            row.label(text="Fingers settings")
            
            box = layout.row()
            col = box.column()
            box = col.box()
            
            col_row = box.row()
            
            if (is_selected(hand_names_flip[0+flip] + hand_names_flip[2+flip] + hand_names_flip[4+flip] + hand_names_flip[6+flip] + hand_names_flip[8+flip] + hand_names_flip[10+flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True) and context.mode == 'POSE':
                col_1 = col_row.column(align = 0)
                col_1a = col_1.row(align = 0)
                col_1a.label(text="")
                col_1a.label(text="", icon=icon_flip[0+flip])
                col_1a.label(text="")
                col_1a = col_1.row(align = 0)
                col_1a.separator()
                col_1a = col_1.row(align = 0)
                col_1a.operator("object.reset_group",text="Reset controls", icon="LOOP_BACK").g_name = 4+flip
                
            if (is_selected(hand_names_flip[1-flip] + hand_names_flip[3-flip] + hand_names_flip[5-flip] + hand_names_flip[7-flip] + hand_names_flip[9-flip] + hand_names_flip[11-flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True) and context.mode == 'POSE':
                col_2 = col_row.column(align = 0)
                col_2a = col_2.row(align = 0)
                col_2a.label(text="")
                col_2a.label(text="", icon=icon_flip[1-flip])
                col_2a.label(text="")
                col_2a = col_2.row(align = 0)
                col_2a.separator()
                col_2a = col_2.row(align = 0)
                col_2a.operator("object.reset_group",text="Reset controls", icon="LOOP_BACK").g_name = 5-flip
                
            row = box.row()
            row.label(text="Fingers IK space:")
            row = box.row()
            
            spaces = ['Master','Hand','Root']
            
            if (is_selected(hand_names_flip[0+flip] + hand_names_flip[2+flip] + hand_names_flip[4+flip] + hand_names_flip[6+flip] + hand_names_flip[8+flip] + hand_names_flip[10+flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True) and context.mode == 'POSE':
                row.prop(bpy.context.active_object.pose.bones[hand_flip[10+flip]],'pole_space', text= spaces[0 + bpy.context.object.pose.bones[hand_flip[10+flip]]["pole_space"]])
                
            if (is_selected(hand_names_flip[1-flip] + hand_names_flip[3-flip] + hand_names_flip[5-flip] + hand_names_flip[7-flip] + hand_names_flip[9-flip] + hand_names_flip[11-flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True) and context.mode == 'POSE':
                row.prop(bpy.context.active_object.pose.bones[hand_flip[11-flip]],'pole_space', text= spaces[0 + bpy.context.object.pose.bones[hand_flip[11-flip]]["pole_space"]])
            
            fngr_ttl = ["Thumb:","Index:","Middle:","Ring:","Pinky:"]
            fngr_nms = [2,4,6,8,10]
            fngr_prps = [0,2,4,6,8]

            if bpy.context.object.pose.bones['properties_panel']["rig_hand_fingers"] == 5:            
                drw_fngr = [0,1,2,3,4]
            else:
                drw_fngr = [0,1,2,4]
            
            for fxgp in drw_fngr: 
                if is_selected(hand_names_flip[fngr_nms[fxgp]] + hand_names_flip[fngr_nms[fxgp]+1]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:
                    
                    row = box.row()
                    row.label(text=fngr_ttl[fxgp])
                    
                    col_row = box.row()
                    
                    if is_selected(hand_names_flip[fngr_nms[fxgp]+flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:
                        col_1 = col_row.column(align = 1)
                        col_1a = col_1.row(align = 1)
                        col_1a.operator("object.select_bone",text="", icon="VIS_SEL_11").bone_name=hand_flip[fngr_prps[fxgp]+flip]
                        col_1a.prop(bpy.context.active_object.pose.bones[hand_flip[fngr_prps[fxgp]+flip]],'IK', text="IK", toggle=True)
                        if IK_double_values == True:
                            col_1a.prop(bpy.context.active_object.pose.bones[hand_flip[fngr_prps[fxgp]+flip]],'IK', text="FK", toggle=True, invert_checkbox=True)
                        col_1a = col_1.row(align = 1)
                        col_1a.prop(bpy.context.active_object.pose.bones[hand_flip[fngr_prps[fxgp]+flip]],'strech', slider= True, text="Strech", emboss = (bpy.context.object.pose.bones[hand_flip[fngr_prps[fxgp]+flip]].IK == True))
                    if is_selected(hand_names_flip[(fngr_nms[fxgp]+1)-flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:    
                        col_2 = col_row.column(align = 1)
                        col_2a = col_2.row(align = 1)
                        col_2a.operator("object.select_bone",text="", icon="VIS_SEL_11").bone_name=hand_flip[(fngr_prps[fxgp]+1)-flip]
                        col_2a.prop(bpy.context.active_object.pose.bones[hand_flip[(fngr_prps[fxgp]+1)-flip]],'IK', text="IK", toggle=True)
                        if IK_double_values == True:
                            col_2a.prop(bpy.context.active_object.pose.bones[hand_flip[(fngr_prps[fxgp]+1)-flip]],'IK', text="FK", toggle=True, invert_checkbox=True)
                        col_2a = col_2.row(align = 1)
                        col_2a.prop(bpy.context.active_object.pose.bones[hand_flip[(fngr_prps[fxgp]+1)-flip]],'strech', slider= True, text="Strech", emboss = (bpy.context.object.pose.bones[hand_flip[(fngr_prps[fxgp]+1)-flip]].IK == True))
                    
            col = layout.column()
                    
#-------------------------- Properties UI Torso
        
        if (bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_settings_torso == True and bpy.context.object.pose.bones['properties_panel'].menu_prop["menu_properties_all"] == True) or (is_selected(torso) and bpy.context.object.pose.bones['properties_panel'].menu_prop["menu_properties_all"] == False):
            
            row = col.row()
            
            row.alignment = 'CENTER'
            row.label(text="Torso settings")
            
            box = layout.row()
            col = box.column()
            box = col.box()
            
            row = box.row() 
            
            row.operator("object.reset_group",text="Reset controls", icon="LOOP_BACK").g_name = 9
            
            row = box.row() 
            row.prop(bpy.context.active_object.pose.bones['properties_torso'],'["bendy_curve"]', text="Spine curvature", slider=True)
            
            col = layout.column()

#-------------------------- Properties UI Legs             

        if (bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_settings_leg == True and bpy.context.object.pose.bones['properties_panel'].menu_prop["menu_properties_all"] == True) or (is_selected(leg_names_flip[0] + leg_names_flip[1]) and bpy.context.object.pose.bones['properties_panel'].menu_prop["menu_properties_all"] == False):
            
            row = col.row()
            
            row.alignment = 'CENTER'
            row.label(text="Legs settings")
            
            box = layout.row()
            col = box.column()
            box = col.box()
            
            col_row = box.row()
            
            if is_selected(leg_names_flip[0+flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:
                
                col_1 = col_row.column(align = 0)
                
                col_1a = col_1.row(align = 0)
                col_1a.label(text="")
                col_1a.operator("object.pick_group",text="", icon=icon_flip[0+flip], emboss = False).p_name = 2+flip
                col_1a.label(text="")
                col_1a = col_1.row(align = 0)
                col_1a.separator()
                col_1a = col_1.row(align = 0)
                col_1a.operator("object.reset_group",text="Reset controls", icon="LOOP_BACK").g_name = 2+flip
                col_1a = col_1.row(align = 0)
                col_1a.separator()
                col_1a = col_1.row(align = 0)
                col_1a.prop(bpy.context.active_object.pose.bones[leg_flip[0+flip]],'no_rot', text=" Follow body", invert_checkbox=True)
                
            if is_selected(leg_names_flip[1-flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:
                
                col_2 = col_row.column(align = 0)
                
                col_2a = col_2.row(align = 0)
                col_2a.label(text="")
                col_2a.operator("object.pick_group",text="", icon=icon_flip[1-flip], emboss = False).p_name = 3-flip
                col_2a.label(text="")
                col_2a = col_2.row(align = 0)
                col_2a.separator()
                col_2a = col_2.row(align = 0)
                col_2a.operator("object.reset_group",text="Reset controls", icon="LOOP_BACK").g_name = 3-flip
                col_2a = col_2.row(align = 0)
                col_2a.separator()
                col_2a = col_2.row(align = 0)
                col_2a.prop(bpy.context.active_object.pose.bones[leg_flip[1-flip]],'no_rot', text=" Follow body", invert_checkbox=True)

            col_row = box.row()

            if is_selected(leg_names_flip[0+flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:
                        
                col_1 = col_row.column(align = 1)
                
                if (bpy.context.object.pose.bones[leg_flip[0+flip]].IK <1) & (bpy.context.object.pose.bones[leg_flip[0+flip]].IK >0):
                    col_1.label(text="need IK 0.0 or 1.0", icon="UNLINKED")
                else:
                    col_1.operator("object.snap",text="Snap & swicht", icon="FILE_REFRESH").s_name = 2+flip
                col_1a = col_1.row(align = 1)
                col_1a.separator()    
                col_1a = col_1.row(align = 1)
                col_1a.operator("object.select_bone",text="", icon="VIS_SEL_11").bone_name=leg_flip[0+flip]
                col_1a.prop(bpy.context.active_object.pose.bones[leg_flip[0+flip]],'IK', toggle=True, text="IK")
                if IK_double_values == True:
                    col_1a.prop(bpy.context.active_object.pose.bones[leg_flip[0+flip]],'IK', toggle=True, text="FK", invert_checkbox=True)
                col_1b = col_1.row(align = 1)
                col_1b.prop(bpy.context.active_object.pose.bones[leg_flip[0+flip]],'strech', slider= True, text="Strech", emboss = (bpy.context.object.pose.bones[leg_flip[0+flip]].IK == True))

            if is_selected(leg_names_flip[1-flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:
                        
                col_2 = col_row.column(align = 1)
                
                if (bpy.context.object.pose.bones[leg_flip[1-flip]].IK <1) & (bpy.context.object.pose.bones[leg_flip[1-flip]].IK >0):
                    col_2.label(text="need IK 0.0 or 1.0", icon="UNLINKED") 
                else:
                    col_2.operator("object.snap",text="Snap & swicht", icon="FILE_REFRESH").s_name = 3-flip
                col_2a = col_2.row(align = 1)
                col_2a.separator()    
                col_2a = col_2.row(align = 1)
                col_2a.operator("object.select_bone",text="", icon="VIS_SEL_11").bone_name=leg_flip[1-flip]
                col_2a.prop(bpy.context.active_object.pose.bones[leg_flip[1-flip]],'IK', toggle=True, text="IK")
                if IK_double_values == True:
                    col_2a.prop(bpy.context.active_object.pose.bones[leg_flip[1-flip]],'IK', toggle=True, text="FK", invert_checkbox = True)
                col_2b = col_2.row(align = 1)
                col_2b.prop(bpy.context.active_object.pose.bones[leg_flip[1-flip]],'strech', slider= True, text="Strech", emboss = (bpy.context.object.pose.bones[leg_flip[1-flip]].IK == True)) 
                
            row = box.row()
            
            row.alignment = 'LEFT'
            row.prop(bpy.context.active_object.pose.bones['properties_panel'].menu_prop,'menu_axis_leg', text="Rotation axis order", toggle=True, icon= submenus[1 - bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_axis_leg], emboss = False)
                
            row = box.row()
            
            axis_grp = ["Thigh", "Shin", "Foot", "Toe"]
            axis_grp2 = ['["axis_order_chnl_3"]', '["axis_order_chnl_2"]', '["axis_order_chnl_1"]', '["axis_order_chnl_0"]']
            axis_grp3 = ["axis_order_chnl_3", "axis_order_chnl_2", "axis_order_chnl_1", "axis_order_chnl_0"]

            drw_ax = [0, 1, 2, 3]
                
            if (bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_axis_leg == False):
                
                for lxgp in drw_ax: 
                    row.label(text=axis_grp[lxgp])
                    
                    row = box.row()
                    
                    if is_selected(leg_names_flip[0+flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:
                               
                        row.prop(bpy.context.active_object.pose.bones[leg_flip[0+flip]],axis_grp2[lxgp], text= euler_channels[0 + bpy.context.object.pose.bones[leg_flip[0+flip]][axis_grp3[lxgp]]])
                        
                    if is_selected(leg_names_flip[1-flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:    
                    
                        row.prop(bpy.context.active_object.pose.bones[leg_flip[1-flip]],axis_grp2[lxgp], text= euler_channels[0 + bpy.context.object.pose.bones[leg_flip[1-flip]][axis_grp3[lxgp]]])
                    
                    row = box.row()
            
            row.alignment = 'LEFT'
            row.prop(bpy.context.active_object.pose.bones['properties_panel'].menu_prop,'menu_pole_leg', text="Pole settings", toggle=True, icon= submenus[1 - bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_pole_leg], emboss = False)

            row = box.row() 
                
            if (bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_pole_leg == False):
                
                if is_selected(leg_names_flip[0+flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:
                        
                    row.prop(bpy.context.active_object.pose.bones[leg_flip[0+flip]],'pole_lock', text=" Pin knee to pole")
                    
                if is_selected(leg_names_flip[1-flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:
                    
                    row.prop(bpy.context.active_object.pose.bones[leg_flip[1-flip]],'pole_lock', text=" Pin knee to pole")
                
                row = box.row()
            
                row.label(text="Space follow")
                
                row = box.row()
                
                spaces = ['Master','Foot','Root']
                
                if is_selected(leg_names_flip[0+flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:
      
                    row.prop(bpy.context.active_object.pose.bones[leg_flip[0+flip]],'pole_space', text= spaces[0 + bpy.context.object.pose.bones[leg_flip[0+flip]]["pole_space"]])
                
                if is_selected(leg_names_flip[1-flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:    

                    row.prop(bpy.context.active_object.pose.bones[leg_flip[1-flip]],'pole_space', text= spaces[0 + bpy.context.object.pose.bones[leg_flip[1-flip]]["pole_space"]])
            
                row = box.row()
            
            row.alignment = 'RIGHT'
            row.prop(bpy.context.active_object.pose.bones['properties_panel'].menu_prop,'advance_settings_legs', text="Advance settings", icon= advance_settings_tabs[0 + bpy.context.object.pose.bones['properties_panel'].menu_prop.advance_settings_legs], emboss = False)
            
            if (bpy.context.object.pose.bones['properties_panel'].menu_prop.advance_settings_legs == True):
                
                col = box.column()
                box = col.box()
                
                row = box.row()
                
                if is_selected(leg_names_flip[0+flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True: 
                
                    row.prop(bpy.context.active_object.pose.bones[leg_flip[0+flip]],'roll_limit', slider= True, text="Toe roll")
                
                if is_selected(leg_names_flip[1-flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:
                    
                    row.prop(bpy.context.active_object.pose.bones[leg_flip[1-flip]],'roll_limit', slider= True, text="Toe roll")
                
                row = box.row()
                
                row.alignment = 'RIGHT'
                row.prop(bpy.context.active_object.pose.bones['properties_panel'],'keyframe_legs', text=' Autokeyframe')
                row = box.row()

                row.alignment = 'CENTER'
                row.label(text="Experimental", icon="EXPERIMENTAL")
                row = box.row()
                
                if is_selected(leg_names_flip[0+flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:
                    
                    row.prop(bpy.context.active_object.pose.bones[leg_flip[0+flip]],'no_pole', invert_checkbox= True, toggle=True, text="Pole", icon= eyes_icon[0 + bpy.context.object.pose.bones[leg_flip[1-flip]]["no_pole"]])
                
                if is_selected(leg_names_flip[1-flip]) or bpy.context.object.pose.bones['properties_panel'].menu_prop.menu_properties_all == True:
                    
                    row.prop(bpy.context.active_object.pose.bones[leg_flip[1-flip]],'no_pole', invert_checkbox= True, toggle=True, text="Pole", icon= eyes_icon[0 + bpy.context.object.pose.bones[leg_flip[1-flip]]["no_pole"]])
                    
            col = layout.column()
            
#-------------------------- Registers

addon_keymaps = []
classes = [MenuProp, OBJECT_OT_SelectBone, OBJECT_OT_Snap, OBJECT_OT_SnapGeneral, OBJECT_OT_PickGroup, OBJECT_OT_ResetGroup, OBJECT_OT_KeyframeProperties, PANEL_PT_RigProperties, OBJECT_MT_ResetSettings]

def register_Panel_Properties_Biped():
    
    for rppb in classes:
        register_class(rppb)
        
    PoseBone.menu_prop = PointerProperty(type=MenuProp, override={'LIBRARY_OVERRIDABLE'})
   
    wm = bpy.context.window_manager
    km = wm.keyconfigs.addon.keymaps.new(name='3D View', space_type='VIEW_3D')
    kmi = km.keymap_items.new(
        OBJECT_OT_SnapGeneral.bl_idname, 
        'X', 'PRESS', shift=False, ctrl=True, alt=False)
    addon_keymaps.append((km, kmi))
    
def unregister_Panel_Properties_Biped():
    
    for rppb in reversed(classes):
        unregister_class(rppb)
    
    del PoseBone.menu_prop
  
    for km, kmi in addon_keymaps:
            km.keymap_items.remove(kmi)
    addon_keymaps.clear()    

if __name__ == "__main__":
    register_Panel_Properties_Biped()
