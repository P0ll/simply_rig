import bpy
from bpy.types import Operator, Panel, PropertyGroup, PoseBone
from bpy.props import IntProperty, BoolProperty, StringProperty, PointerProperty, EnumProperty

#-------------------------- ID

rig_id = "simply_rig"
rig_type = "biped"

#-------------------------- Tabs 
 
ui_list = ['Item', 'Simply rig UI'] # space avaibles to draw the panel
ui_location = ui_list[1] # this value chooses the location to run the panel

#-------------------------- Icon style

submenus = ["RIGHTARROW", "DOWNARROW_HLT"]

#-------------------------- Flip Groups
                            # important no change the order for the (1º)left and (2º)right groups
                                                        
icon_flip = ['EVENT_L','EVENT_R']
 
#-------------------------- Functions

def select_op(self, context, event, bone_name):
    armobj = bpy.context.active_object
    arm = bpy.context.active_object.data
    
    if (bone_name in armobj.pose.bones):
        Bone = armobj.pose.bones[bone_name]

        if event.ctrl == True or event.shift == True:
            selected = [b.name for b in bpy.context.selected_pose_bones]

            for b in armobj.pose.bones:
                b.bone.select = 0
            arm.bones.active = Bone.bone
            
            for b in armobj.pose.bones:
                if (b.name in selected):
                    b.bone.select = 1
        else:
            for b in armobj.pose.bones:
                b.bone.select = 0
            arm.bones.active = Bone.bone
            
#-------------------------- Operators

class OBJECT_OT_ToggleProp(Operator):
    
    """toggle prop"""
    
    bl_idname = "object.toggle_prop" 
    bl_label = ""
    sf_prop: StringProperty(name="")
    
    def execute(self, context):
        
        if (bpy.context.object.pose.bones['properties_panel'].menu_pick[self.sf_prop]==True):
            bpy.context.object.pose.bones['properties_panel'].menu_pick[self.sf_prop] = False
        else:
            bpy.context.object.pose.bones['properties_panel'].menu_pick[self.sf_prop] = True
            
        return {'FINISHED'} 

class OBJECT_OT_PICKER_Bones(Operator):
    
    """Pick bones"""
    
    bl_idname = "object.pick_bone" 
    bl_label = "Pick bones"
    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}
    bone_name: StringProperty(name="bone_name")

    def invoke(self, context, event):
        select_op(self, context, event, self.bone_name)
        return {"FINISHED"}
    
#-------------------------- Panel Properties
    
class MenuPick(PropertyGroup):
    
    list_picker: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    
    g_head: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    
    g_arm_L: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    g_arm_R: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    
    g_fingers_L: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    g_fingers_R: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    
    g_h_thumb_L: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    g_h_thumb_R: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    
    g_h_index_L: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    g_h_index_R: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    
    g_h_middle_L: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    g_h_middle_R: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    
    g_h_ring_L: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    g_h_ring_R: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    
    g_h_pinky_L: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    g_h_pinky_R: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    
    g_torso: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    
    g_leg_L: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    g_leg_R: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    
    g_master: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    
#-------------------------- Picker UI

class PANEL_PT_PickerBody(Panel):
    
    """Draw panels for pick controls"""
    
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = ui_location
    bl_label = ""
    
    def draw_header(self, context):
        self.layout.label(text=bpy.data.objects[bpy.context.active_object.name].pose.bones["properties_panel"]["character_name"] + " Picker", icon="RESTRICT_SELECT_OFF")

    @classmethod
    def poll(self, context):
        if context.mode != 'POSE':
            return False

        try:
           ob = context.active_object
           return (context.active_object.data.get("rig_id") == rig_id) and (context.active_object.data.get("rig_type") == rig_type)
        except AttributeError:
           return 0
       
    def draw(self, context):
        flip = bpy.context.scene.menu_propscn.flip
        
        layout = self.layout
#        col = layout.column()
#        
#        row = col.row()
#        
#        row = col.row(align=1)
#        
#        row.prop(bpy.context.active_object.pose.bones['properties_panel'].menu_pick,'list_picker', text="Drawing view", icon="POSE_HLT", toggle=True, invert_checkbox = True)
#        row.prop(bpy.context.active_object.pose.bones['properties_panel'].menu_pick,'list_picker', text="List view", icon="OUTLINER", toggle=True)
        
#        row = col.row()
#        row.separator()
#        row = col.row()
        
        col = layout.row().column()
        
#        row.prop(context.active_object.pose.bones['properties_panel'].menu_pick,'g_master', text="test", toggle=True)

#        if (bpy.context.object.pose.bones['properties_panel'].menu_pick.list_picker == True):
            
        col_row = col.row()
        col_1 = col_row.column(align=1)
        
#        col_1.operator('object.toggle_prop',text="Head", icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_head']]).sf_prop="g_head"
        col_1.box().prop(context.active_object.pose.bones['properties_panel'].menu_pick,'g_head', text="Head", toggle=True, emboss=False, icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_head']])
        
        if (bpy.context.object.pose.bones['properties_panel'].menu_pick['g_head'] == True):
            
            col_loc = col_1.box()
            
            col_loc.operator('object.pick_bone',text="Head toon",depress=(bpy.context.object.pose.bones["head_toon"].bone.select==True)).bone_name= "head_toon"
            col_loc.operator('object.pick_bone',text="Head",depress=(bpy.context.object.pose.bones["head"].bone.select==True)).bone_name= "head"
            col_loc.operator('object.pick_bone',text="Head position",depress=(bpy.context.object.pose.bones["head_rot"].bone.select==True)).bone_name= "head_rot"
            col_loc.operator('object.pick_bone',text="Neck",depress=(bpy.context.object.pose.bones["neck_rot"].bone.select==True)).bone_name= "neck_rot"
            
        col_row = col.row()
        
        if flip==1:
            col_2 = col_row.column(align=1)
            col_1 = col_row.column(align=1)
        else:
            col_1 = col_row.column(align=1)
            col_2 = col_row.column(align=1)
            
#        col_1.operator('object.toggle_prop',text="Arm L", icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_arm_L']]).sf_prop="g_arm_L"
        col_1.box().prop(context.active_object.pose.bones['properties_panel'].menu_pick,'g_arm_L', text="Arm L", toggle=True, emboss=False, icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_arm_L']])
     
        if (bpy.context.object.pose.bones['properties_panel'].menu_pick['g_arm_L'] == True):
            
            col_loc = col_1.box()
            
            col_loc.operator('object.pick_bone',text="Shoulder",depress=(bpy.context.object.pose.bones["shoulder_L"].bone.select==True)).bone_name= "shoulder_L"
            
            if (bpy.context.object.pose.bones["properties_arm_L"].IK == 0):
                col_loc.operator('object.pick_bone',text="Arm General",depress=(bpy.context.object.pose.bones["arm_FK_L"].bone.select==True)).bone_name= "arm_FK_L"
                col_loc.operator('object.pick_bone',text="Upperarm",depress=(bpy.context.object.pose.bones["upperarm_FK_L"].bone.select==True)).bone_name= "upperarm_FK_L"
                col_loc.operator('object.pick_bone',text="Forearm",depress=(bpy.context.object.pose.bones["forearm_FK_L"].bone.select==True)).bone_name= "forearm_FK_L"
                col_loc.operator('object.pick_bone',text="Hand",depress=(bpy.context.object.pose.bones["hand_FK_L"].bone.select==True)).bone_name= "hand_FK_L"
            else:
                col_loc.operator('object.pick_bone',text="IK Pivot",depress=(bpy.context.object.pose.bones["hand_IK_pivot_L"].bone.select==True)).bone_name= "hand_IK_pivot_L"
                col_loc.operator('object.pick_bone',text="Upperarm",depress=(bpy.context.object.pose.bones["upperarm_IK_L"].bone.select==True)).bone_name= "upperarm_IK_L"
                col_loc.operator('object.pick_bone',text="Elbow pole",depress=(bpy.context.object.pose.bones["elbow_pole_L"].bone.select==True)).bone_name= "elbow_pole_L"
                col_loc.operator('object.pick_bone',text="Forearm",depress=(bpy.context.object.pose.bones["forearm_IK_L"].bone.select==True)).bone_name= "forearm_IK_L"
                col_loc.operator('object.pick_bone',text="Hand",depress=(bpy.context.object.pose.bones["hand_IK_L"].bone.select==True)).bone_name= "hand_IK_L"
            
            col_loc.operator('object.pick_bone',text="Upperarm Toon",depress=(bpy.context.object.pose.bones["arm_toon_3_L"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_panel"].layer_toon_arm_L == True)).bone_name= "arm_toon_3_L"
            col_loc.operator('object.pick_bone',text="Elbow Toon",depress=(bpy.context.object.pose.bones["arm_toon_2_L"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_panel"].layer_toon_arm_L == True)).bone_name= "arm_toon_2_L"
            col_loc.operator('object.pick_bone',text="Forearm Toon",depress=(bpy.context.object.pose.bones["arm_toon_1_L"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_panel"].layer_toon_arm_L == True)).bone_name= "arm_toon_1_L"
                
#        col_2.operator('object.toggle_prop',text="Arm R", icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_arm_R']]).sf_prop="g_arm_R"
        col_2.box().prop(context.active_object.pose.bones['properties_panel'].menu_pick,'g_arm_R', text="Arm R", toggle=True, emboss=False, icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_arm_R']])
        
        if (bpy.context.object.pose.bones['properties_panel'].menu_pick['g_arm_R'] == True):
            
            col_loc = col_2.box()
            
            col_loc.operator('object.pick_bone',text="Shoulder",depress=(bpy.context.object.pose.bones["shoulder_R"].bone.select==True)).bone_name= "shoulder_R"
            
            if (bpy.context.object.pose.bones["properties_arm_R"].IK == 0):
                col_loc.operator('object.pick_bone',text="Arm General",depress=(bpy.context.object.pose.bones["arm_FK_R"].bone.select==True)).bone_name= "arm_FK_R"
                col_loc.operator('object.pick_bone',text="Upperarm",depress=(bpy.context.object.pose.bones["upperarm_FK_R"].bone.select==True)).bone_name= "upperarm_FK_R"
                col_loc.operator('object.pick_bone',text="Forearm",depress=(bpy.context.object.pose.bones["forearm_FK_R"].bone.select==True)).bone_name= "forearm_FK_R"
                col_loc.operator('object.pick_bone',text="Hand",depress=(bpy.context.object.pose.bones["hand_FK_R"].bone.select==True)).bone_name= "hand_FK_R"
            else:
                col_loc.operator('object.pick_bone',text="IK Pivot",depress=(bpy.context.object.pose.bones["hand_IK_pivot_R"].bone.select==True)).bone_name= "hand_IK_pivot_R"
                col_loc.operator('object.pick_bone',text="Upperarm",depress=(bpy.context.object.pose.bones["upperarm_IK_R"].bone.select==True)).bone_name= "upperarm_IK_R"
                col_loc.operator('object.pick_bone',text="Elbow pole",depress=(bpy.context.object.pose.bones["elbow_pole_R"].bone.select==True)).bone_name= "elbow_pole_R"
                col_loc.operator('object.pick_bone',text="Forearm",depress=(bpy.context.object.pose.bones["forearm_IK_R"].bone.select==True)).bone_name= "forearm_IK_R"
                col_loc.operator('object.pick_bone',text="Hand",depress=(bpy.context.object.pose.bones["hand_IK_R"].bone.select==True)).bone_name= "hand_IK_R"
            
            col_loc.operator('object.pick_bone',text="Upperarm Toon",depress=(bpy.context.object.pose.bones["arm_toon_3_R"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_panel"].layer_toon_arm_R == True)).bone_name= "arm_toon_3_R"
            col_loc.operator('object.pick_bone',text="Elbow Toon",depress=(bpy.context.object.pose.bones["arm_toon_2_R"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_panel"].layer_toon_arm_R == True)).bone_name= "arm_toon_2_R"
            col_loc.operator('object.pick_bone',text="Forearm Toon",depress=(bpy.context.object.pose.bones["arm_toon_1_R"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_panel"].layer_toon_arm_R == True)).bone_name= "arm_toon_1_R"
        
        col_row = col.row()
        
        if flip==1:
            col_2 = col_row.column(align=1)
            col_1 = col_row.column(align=1)
        else:
            col_1 = col_row.column(align=1)
            col_2 = col_row.column(align=1)
        
        col_1.box().prop(context.active_object.pose.bones['properties_panel'].menu_pick,'g_fingers_L', text="Fingers L", toggle=True, emboss=False, icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_fingers_L']])
            
        if (bpy.context.object.pose.bones['properties_panel'].menu_pick['g_fingers_L'] == True):
            
            col_loc = col_1.box()
            
            col_loc.operator('object.pick_bone',text="Fingers general",depress=(bpy.context.object.pose.bones["fingers_L"].bone.select==True)).bone_name= "fingers_L"
            col_loc.operator('object.pick_bone',text="Fingers IK",depress=(bpy.context.object.pose.bones["fingers_IK_L"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_fh_thumb_L"].IK == True) or (bpy.context.object.pose.bones["properties_fh_index_L"].IK == True) or (bpy.context.object.pose.bones["properties_fh_middle_L"].IK == True) or (bpy.context.object.pose.bones["properties_fh_ring_L"].IK == True) or (bpy.context.object.pose.bones["properties_fh_pinky_L"].IK == True)).bone_name= "fingers_IK_L"
            
            col_loc.prop(context.active_object.pose.bones['properties_panel'].menu_pick,'g_h_thumb_L', text="Thumb", toggle=True, emboss=False, icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_h_thumb_L']])
            
            if (bpy.context.object.pose.bones['properties_panel'].menu_pick['g_h_thumb_L'] == True):
                
                col_loc.operator('object.pick_bone',text="Thumb general",depress=(bpy.context.object.pose.bones["fh_thumb_general_L"].bone.select==True)).bone_name= "fh_thumb_general_L"
                col_loc.operator('object.pick_bone',text="Thumb 3",depress=(bpy.context.object.pose.bones["fh_thumb_3_L"].bone.select==True)).bone_name= "fh_thumb_3_L"
                col_loc.operator('object.pick_bone',text="Thumb 2",depress=(bpy.context.object.pose.bones["fh_thumb_2_L"].bone.select==True)).bone_name= "fh_thumb_2_L"
                col_loc.operator('object.pick_bone',text="Thumb 1",depress=(bpy.context.object.pose.bones["fh_thumb_1_L"].bone.select==True)).bone_name= "fh_thumb_1_L"
                col_loc.operator('object.pick_bone',text="Thumb IK",depress=(bpy.context.object.pose.bones["fh_thumb_IK_L"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_fh_thumb_L"].IK == True)).bone_name= "fh_thumb_IK_L"
                col_loc.operator('object.pick_bone',text="Thumb deformer",depress=(bpy.context.object.pose.bones["fh_thumb_deformer_L"].bone.select==True)).bone_name= "fh_thumb_deformer_L"
            
            col_loc.prop(context.active_object.pose.bones['properties_panel'].menu_pick,'g_h_index_L', text="Index", toggle=True, emboss=False, icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_h_index_L']])
            
            if (bpy.context.object.pose.bones['properties_panel'].menu_pick['g_h_index_L'] == True):
                
                col_loc.operator('object.pick_bone',text="Index 4",depress=(bpy.context.object.pose.bones["fh_index_4_L"].bone.select==True)).bone_name= "fh_index_4_L"
                col_loc.operator('object.pick_bone',text="Index general",depress=(bpy.context.object.pose.bones["fh_index_general_L"].bone.select==True)).bone_name= "fh_index_general_L"
                col_loc.operator('object.pick_bone',text="Index 3",depress=(bpy.context.object.pose.bones["fh_index_3_L"].bone.select==True)).bone_name= "fh_index_3_L"
                col_loc.operator('object.pick_bone',text="Index 2",depress=(bpy.context.object.pose.bones["fh_index_2_L"].bone.select==True)).bone_name= "fh_index_2_L"
                col_loc.operator('object.pick_bone',text="Index 1",depress=(bpy.context.object.pose.bones["fh_index_1_L"].bone.select==True)).bone_name= "fh_index_1_L"
                col_loc.operator('object.pick_bone',text="Index IK",depress=(bpy.context.object.pose.bones["fh_index_IK_L"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_fh_index_L"].IK == True)).bone_name= "fh_index_IK_L"
                col_loc.operator('object.pick_bone',text="Index deformer",depress=(bpy.context.object.pose.bones["fh_index_deformer_L"].bone.select==True)).bone_name= "fh_index_deformer_L"
            
            col_loc.prop(context.active_object.pose.bones['properties_panel'].menu_pick,'g_h_middle_L', text="Middle", toggle=True, emboss=False, icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_h_middle_L']])
            
            if (bpy.context.object.pose.bones['properties_panel'].menu_pick['g_h_middle_L'] == True):
                
                col_loc.operator('object.pick_bone',text="Middle 4",depress=(bpy.context.object.pose.bones["fh_middle_4_L"].bone.select==True)).bone_name= "fh_middle_4_L"
                col_loc.operator('object.pick_bone',text="Middle general",depress=(bpy.context.object.pose.bones["fh_middle_general_L"].bone.select==True)).bone_name= "fh_middle_general_L"
                col_loc.operator('object.pick_bone',text="Middle 3",depress=(bpy.context.object.pose.bones["fh_middle_3_L"].bone.select==True)).bone_name= "fh_middle_3_L"
                col_loc.operator('object.pick_bone',text="Middle 2",depress=(bpy.context.object.pose.bones["fh_middle_2_L"].bone.select==True)).bone_name= "fh_middle_2_L"
                col_loc.operator('object.pick_bone',text="Middle 1",depress=(bpy.context.object.pose.bones["fh_middle_1_L"].bone.select==True)).bone_name= "fh_middle_1_L"
                col_loc.operator('object.pick_bone',text="Middle IK",depress=(bpy.context.object.pose.bones["fh_middle_IK_L"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_fh_middle_L"].IK == True)).bone_name= "fh_middle_IK_L"
                col_loc.operator('object.pick_bone',text="Middle deformer",depress=(bpy.context.object.pose.bones["fh_middle_deformer_L"].bone.select==True)).bone_name= "fh_middle_deformer_L"

            if bpy.context.object.pose.bones['properties_panel']["rig_hand_fingers"] == 5:
            
                col_loc.prop(context.active_object.pose.bones['properties_panel'].menu_pick,'g_h_ring_L', text="Ring", toggle=True, emboss=False, icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_h_ring_L']])
            
                if (bpy.context.object.pose.bones['properties_panel'].menu_pick['g_h_ring_L'] == True):
                    
                    col_loc.operator('object.pick_bone',text="Ring 4",depress=(bpy.context.object.pose.bones["fh_ring_4_L"].bone.select==True)).bone_name= "fh_ring_4_L"
                    col_loc.operator('object.pick_bone',text="Ring general",depress=(bpy.context.object.pose.bones["fh_ring_general_L"].bone.select==True)).bone_name= "fh_ring_general_L"
                    col_loc.operator('object.pick_bone',text="Ring 3",depress=(bpy.context.object.pose.bones["fh_ring_3_L"].bone.select==True)).bone_name= "fh_ring_3_L"
                    col_loc.operator('object.pick_bone',text="Ring 2",depress=(bpy.context.object.pose.bones["fh_ring_2_L"].bone.select==True)).bone_name= "fh_ring_2_L"
                    col_loc.operator('object.pick_bone',text="Ring 1",depress=(bpy.context.object.pose.bones["fh_ring_1_L"].bone.select==True)).bone_name= "fh_ring_1_L"
                    col_loc.operator('object.pick_bone',text="Ring IK",depress=(bpy.context.object.pose.bones["fh_ring_IK_L"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_fh_ring_L"].IK == True)).bone_name= "fh_ring_IK_L"
                    col_loc.operator('object.pick_bone',text="Ring deformer",depress=(bpy.context.object.pose.bones["fh_ring_deformer_L"].bone.select==True)).bone_name= "fh_ring_deformer_L"
            
            col_loc.prop(context.active_object.pose.bones['properties_panel'].menu_pick,'g_h_pinky_L', text="Pinky", toggle=True, emboss=False, icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_h_pinky_L']])
            
            if (bpy.context.object.pose.bones['properties_panel'].menu_pick['g_h_pinky_L'] == True):
                
                col_loc.operator('object.pick_bone',text="Pinky 4",depress=(bpy.context.object.pose.bones["fh_pinky_4_L"].bone.select==True)).bone_name= "fh_pinky_4_L"
                col_loc.operator('object.pick_bone',text="Pinky general",depress=(bpy.context.object.pose.bones["fh_pinky_general_L"].bone.select==True)).bone_name= "fh_pinky_general_L"
                col_loc.operator('object.pick_bone',text="Pinky 3",depress=(bpy.context.object.pose.bones["fh_pinky_3_L"].bone.select==True)).bone_name= "fh_pinky_3_L"
                col_loc.operator('object.pick_bone',text="Pinky 2",depress=(bpy.context.object.pose.bones["fh_pinky_2_L"].bone.select==True)).bone_name= "fh_pinky_2_L"
                col_loc.operator('object.pick_bone',text="Pinky 1",depress=(bpy.context.object.pose.bones["fh_pinky_1_L"].bone.select==True)).bone_name= "fh_pinky_1_L"
                col_loc.operator('object.pick_bone',text="Pinky IK",depress=(bpy.context.object.pose.bones["fh_pinky_IK_L"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_fh_pinky_L"].IK == True)).bone_name= "fh_pinky_IK_L"
                col_loc.operator('object.pick_bone',text="Pinky deformer",depress=(bpy.context.object.pose.bones["fh_pinky_deformer_L"].bone.select==True)).bone_name= "fh_pinky_deformer_L"
            
        col_2.box().prop(context.active_object.pose.bones['properties_panel'].menu_pick,'g_fingers_R', text="Fingers R", toggle=True, emboss=False, icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_fingers_R']])
        
        if (bpy.context.object.pose.bones['properties_panel'].menu_pick['g_fingers_R'] == True):
            
            col_loc = col_2.box()
            
            col_loc.operator('object.pick_bone',text="Fingers general",depress=(bpy.context.object.pose.bones["fingers_R"].bone.select==True)).bone_name= "fingers_R"
            col_loc.operator('object.pick_bone',text="Fingers IK",depress=(bpy.context.object.pose.bones["fingers_IK_R"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_fh_thumb_R"].IK == True) or (bpy.context.object.pose.bones["properties_fh_index_R"].IK == True) or (bpy.context.object.pose.bones["properties_fh_middle_R"].IK == True) or (bpy.context.object.pose.bones["properties_fh_ring_R"].IK == True) or (bpy.context.object.pose.bones["properties_fh_pinky_R"].IK == True)).bone_name= "fingers_IK_R"
            
            col_loc.prop(context.active_object.pose.bones['properties_panel'].menu_pick,'g_h_thumb_R', text="Thumb", toggle=True, emboss=False, icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_h_thumb_R']])
            
            if (bpy.context.object.pose.bones['properties_panel'].menu_pick['g_h_thumb_R'] == True):
                
                col_loc.operator('object.pick_bone',text="Thumb general",depress=(bpy.context.object.pose.bones["fh_thumb_general_R"].bone.select==True)).bone_name= "fh_thumb_general_R"
                col_loc.operator('object.pick_bone',text="Thumb 3",depress=(bpy.context.object.pose.bones["fh_thumb_3_R"].bone.select==True)).bone_name= "fh_thumb_3_R"
                col_loc.operator('object.pick_bone',text="Thumb 2",depress=(bpy.context.object.pose.bones["fh_thumb_2_R"].bone.select==True)).bone_name= "fh_thumb_2_R"
                col_loc.operator('object.pick_bone',text="Thumb 1",depress=(bpy.context.object.pose.bones["fh_thumb_1_R"].bone.select==True)).bone_name= "fh_thumb_1_R"
                col_loc.operator('object.pick_bone',text="Thumb IK",depress=(bpy.context.object.pose.bones["fh_thumb_IK_R"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_fh_thumb_R"].IK == True)).bone_name= "fh_thumb_IK_R"
                col_loc.operator('object.pick_bone',text="Thumb deformer",depress=(bpy.context.object.pose.bones["fh_thumb_deformer_R"].bone.select==True)).bone_name= "fh_thumb_deformer_R"
            
            col_loc.prop(context.active_object.pose.bones['properties_panel'].menu_pick,'g_h_index_R', text="Index", toggle=True, emboss=False, icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_h_index_R']])
            
            if (bpy.context.object.pose.bones['properties_panel'].menu_pick['g_h_index_R'] == True):
                
                col_loc.operator('object.pick_bone',text="Index 4",depress=(bpy.context.object.pose.bones["fh_index_4_R"].bone.select==True)).bone_name= "fh_index_4_R"
                col_loc.operator('object.pick_bone',text="Index general",depress=(bpy.context.object.pose.bones["fh_index_general_R"].bone.select==True)).bone_name= "fh_index_general_R"
                col_loc.operator('object.pick_bone',text="Index 3",depress=(bpy.context.object.pose.bones["fh_index_3_R"].bone.select==True)).bone_name= "fh_index_3_R"
                col_loc.operator('object.pick_bone',text="Index 2",depress=(bpy.context.object.pose.bones["fh_index_2_R"].bone.select==True)).bone_name= "fh_index_2_R"
                col_loc.operator('object.pick_bone',text="Index 1",depress=(bpy.context.object.pose.bones["fh_index_1_R"].bone.select==True)).bone_name= "fh_index_1_R"
                col_loc.operator('object.pick_bone',text="Index IK",depress=(bpy.context.object.pose.bones["fh_index_IK_R"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_fh_index_R"].IK == True)).bone_name= "fh_index_IK_R"
                col_loc.operator('object.pick_bone',text="Index deformer",depress=(bpy.context.object.pose.bones["fh_index_deformer_R"].bone.select==True)).bone_name= "fh_index_deformer_R"
            
            col_loc.prop(context.active_object.pose.bones['properties_panel'].menu_pick,'g_h_middle_R', text="Middle", toggle=True, emboss=False, icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_h_middle_R']])
            
            if (bpy.context.object.pose.bones['properties_panel'].menu_pick['g_h_middle_R'] == True):
                
                col_loc.operator('object.pick_bone',text="Middle 4",depress=(bpy.context.object.pose.bones["fh_middle_4_R"].bone.select==True)).bone_name= "fh_middle_4_R"
                col_loc.operator('object.pick_bone',text="Middle general",depress=(bpy.context.object.pose.bones["fh_middle_general_R"].bone.select==True)).bone_name= "fh_middle_general_R"
                col_loc.operator('object.pick_bone',text="Middle 3",depress=(bpy.context.object.pose.bones["fh_middle_3_R"].bone.select==True)).bone_name= "fh_middle_3_R"
                col_loc.operator('object.pick_bone',text="Middle 2",depress=(bpy.context.object.pose.bones["fh_middle_2_R"].bone.select==True)).bone_name= "fh_middle_2_R"
                col_loc.operator('object.pick_bone',text="Middle 1",depress=(bpy.context.object.pose.bones["fh_middle_1_R"].bone.select==True)).bone_name= "fh_middle_1_R"
                col_loc.operator('object.pick_bone',text="Middle IK",depress=(bpy.context.object.pose.bones["fh_middle_IK_R"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_fh_middle_R"].IK == True)).bone_name= "fh_middle_IK_R"
                col_loc.operator('object.pick_bone',text="Middle deformer",depress=(bpy.context.object.pose.bones["fh_middle_deformer_R"].bone.select==True)).bone_name= "fh_middle_deformer_R"
            
            if bpy.context.object.pose.bones['properties_panel']["rig_hand_fingers"] == 5:
                
                col_loc.prop(context.active_object.pose.bones['properties_panel'].menu_pick,'g_h_ring_R', text="Ring", toggle=True, emboss=False, icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_h_ring_R']])
                
                if (bpy.context.object.pose.bones['properties_panel'].menu_pick['g_h_ring_R'] == True):
                    
                    col_loc.operator('object.pick_bone',text="Ring 4",depress=(bpy.context.object.pose.bones["fh_ring_4_R"].bone.select==True)).bone_name= "fh_ring_4_R"
                    col_loc.operator('object.pick_bone',text="Ring general",depress=(bpy.context.object.pose.bones["fh_ring_general_R"].bone.select==True)).bone_name= "fh_ring_general_R"
                    col_loc.operator('object.pick_bone',text="Ring 3",depress=(bpy.context.object.pose.bones["fh_ring_3_R"].bone.select==True)).bone_name= "fh_ring_3_R"
                    col_loc.operator('object.pick_bone',text="Ring 2",depress=(bpy.context.object.pose.bones["fh_ring_2_R"].bone.select==True)).bone_name= "fh_ring_2_R"
                    col_loc.operator('object.pick_bone',text="Ring 1",depress=(bpy.context.object.pose.bones["fh_ring_1_R"].bone.select==True)).bone_name= "fh_ring_1_R"
                    col_loc.operator('object.pick_bone',text="Ring IK",depress=(bpy.context.object.pose.bones["fh_ring_IK_R"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_fh_ring_R"].IK == True)).bone_name= "fh_ring_IK_R"
                    col_loc.operator('object.pick_bone',text="Ring deformer",depress=(bpy.context.object.pose.bones["fh_ring_deformer_R"].bone.select==True)).bone_name= "fh_ring_deformer_R"
            
            col_loc.prop(context.active_object.pose.bones['properties_panel'].menu_pick,'g_h_pinky_R', text="Pinky", toggle=True, emboss=False, icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_h_pinky_R']])
            
            if (bpy.context.object.pose.bones['properties_panel'].menu_pick['g_h_pinky_R'] == True):
                
                col_loc.operator('object.pick_bone',text="Pinky 4",depress=(bpy.context.object.pose.bones["fh_pinky_4_R"].bone.select==True)).bone_name= "fh_pinky_4_R"
                col_loc.operator('object.pick_bone',text="Pinky general",depress=(bpy.context.object.pose.bones["fh_pinky_general_R"].bone.select==True)).bone_name= "fh_pinky_general_R"
                col_loc.operator('object.pick_bone',text="Pinky 3",depress=(bpy.context.object.pose.bones["fh_pinky_3_R"].bone.select==True)).bone_name= "fh_pinky_3_R"
                col_loc.operator('object.pick_bone',text="Pinky 2",depress=(bpy.context.object.pose.bones["fh_pinky_2_R"].bone.select==True)).bone_name= "fh_pinky_2_R"
                col_loc.operator('object.pick_bone',text="Pinky 1",depress=(bpy.context.object.pose.bones["fh_pinky_1_R"].bone.select==True)).bone_name= "fh_pinky_1_R"
                col_loc.operator('object.pick_bone',text="Pinky IK",depress=(bpy.context.object.pose.bones["fh_pinky_IK_R"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_fh_pinky_R"].IK == True)).bone_name= "fh_pinky_IK_R"
                col_loc.operator('object.pick_bone',text="Pinky deformer",depress=(bpy.context.object.pose.bones["fh_pinky_deformer_R"].bone.select==True)).bone_name= "fh_pinky_deformer_R"
        
        col_row = col.row()
        col_1 = col_row.column(align=1)
        
#        col_1.operator('object.toggle_prop',text="Torso", icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_torso']]).sf_prop="g_torso"
        col_1.box().prop(context.active_object.pose.bones['properties_panel'].menu_pick,'g_torso', text="Torso", toggle=True, emboss=False, icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_torso']])
        
        if (bpy.context.object.pose.bones['properties_panel'].menu_pick['g_torso'] == True):
            
            col_loc = col_1.box()
            
            col_loc.operator('object.pick_bone',text="Chest IK",depress=(bpy.context.object.pose.bones["chest_IK"].bone.select==True)).bone_name= "chest_IK"
            col_loc.operator('object.pick_bone',text="Chest FK",depress=(bpy.context.object.pose.bones["chest_FK"].bone.select==True)).bone_name= "chest_FK"
            col_loc.operator('object.pick_bone',text="Spine 1",depress=(bpy.context.object.pose.bones["spine_FK_1"].bone.select==True)).bone_name= "spine_FK_1"
            col_loc.operator('object.pick_bone',text="Spine 1 inv",depress=(bpy.context.object.pose.bones["spine_FK_inv_1"].bone.select==True)).bone_name= "spine_FK_inv_1"
            col_loc.operator('object.pick_bone',text="Spine Toon",depress=(bpy.context.object.pose.bones["spine_toon"].bone.select==True)).bone_name= "spine_toon"
            col_loc.operator('object.pick_bone',text="Spine 2",depress=(bpy.context.object.pose.bones["spine_FK_2"].bone.select==True)).bone_name= "spine_FK_2"
            col_loc.operator('object.pick_bone',text="Spine 2 inv",depress=(bpy.context.object.pose.bones["spine_FK_inv_2"].bone.select==True)).bone_name= "spine_FK_inv_2"
            col_loc.operator('object.pick_bone',text="Hip IK",depress=(bpy.context.object.pose.bones["hip_IK"].bone.select==True)).bone_name= "hip_IK"
            col_loc.operator('object.pick_bone',text="Hip FK",depress=(bpy.context.object.pose.bones["hip_FK"].bone.select==True)).bone_name= "hip_FK"
            col_loc.operator('object.pick_bone',text="Master torso",depress=(bpy.context.object.pose.bones["master_torso"].bone.select==True)).bone_name= "master_torso"
            
            col_loc.operator('object.pick_bone',text="Virtual pivot",depress=(bpy.context.object.pose.bones["master_torso_pivot"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_panel"].virtual_pivot_torso == True)).bone_name= "master_torso_pivot"
            col_loc.operator('object.pick_bone',text="Virtual master torso",depress=(bpy.context.object.pose.bones["master_torso_pivot_target"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_panel"].virtual_pivot_torso == True)).bone_name= "master_torso_pivot_target"
        
        col_row = col.row()
        
        if flip==1:
            col_2 = col_row.column(align=1)
            col_1 = col_row.column(align=1)
        else:
            col_1 = col_row.column(align=1)
            col_2 = col_row.column(align=1)
        
#        col_1.operator('object.toggle_prop',text="Leg L", icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_leg_L']]).sf_prop="g_leg_L"
        col_1.box().prop(context.active_object.pose.bones['properties_panel'].menu_pick,'g_leg_L', text="Leg L", toggle=True, emboss=False, icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_leg_L']])
            
        if (bpy.context.object.pose.bones['properties_panel'].menu_pick['g_leg_L'] == True):
            
            col_loc = col_1.box()
            
            if (bpy.context.object.pose.bones["properties_leg_L"].IK == 0):
                col_loc.operator('object.pick_bone',text="Leg General",depress=(bpy.context.object.pose.bones["leg_FK_L"].bone.select==True)).bone_name= "leg_FK_L"
                col_loc.operator('object.pick_bone',text="Thigh",depress=(bpy.context.object.pose.bones["thigh_FK_L"].bone.select==True)).bone_name= "thigh_FK_L"
                col_loc.operator('object.pick_bone',text="Shin",depress=(bpy.context.object.pose.bones["shin_FK_L"].bone.select==True)).bone_name= "shin_FK_L"
                col_loc.operator('object.pick_bone',text="Foot",depress=(bpy.context.object.pose.bones["foot_FK_L"].bone.select==True)).bone_name= "foot_FK_L"
            else:
                col_loc.operator('object.pick_bone',text="IK Pivot",depress=(bpy.context.object.pose.bones["foot_IK_pivot_L"].bone.select==True)).bone_name= "foot_IK_pivot_L"
                col_loc.operator('object.pick_bone',text="Thight",depress=(bpy.context.object.pose.bones["thigh_IK_L"].bone.select==True)).bone_name= "thigh_IK_L"
                col_loc.operator('object.pick_bone',text="Knee pole",depress=(bpy.context.object.pose.bones["knee_pole_L"].bone.select==True)).bone_name= "knee_pole_L"
                col_loc.operator('object.pick_bone',text="Shin",depress=(bpy.context.object.pose.bones["shin_IK_L"].bone.select==True)).bone_name= "shin_IK_L"
                col_loc.operator('object.pick_bone',text="Ankle",depress=(bpy.context.object.pose.bones["ankle_roll_L"].bone.select==True)).bone_name= "ankle_roll_L"
                col_loc.operator('object.pick_bone',text="Foot",depress=(bpy.context.object.pose.bones["foot_IK_L"].bone.select==True)).bone_name= "foot_IK_L"
            
            col_loc.operator('object.pick_bone',text="Toe",depress=(bpy.context.object.pose.bones["toe_L"].bone.select==True)).bone_name= "toe_L"
            
            col_loc.operator('object.pick_bone',text="Thigh Toon",depress=(bpy.context.object.pose.bones["leg_toon_3_L"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_panel"].layer_toon_leg_L == True)).bone_name= "leg_toon_3_L"
            col_loc.operator('object.pick_bone',text="Knee Toon",depress=(bpy.context.object.pose.bones["leg_toon_2_L"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_panel"].layer_toon_leg_L == True)).bone_name= "leg_toon_2_L"
            col_loc.operator('object.pick_bone',text="Shin Toon",depress=(bpy.context.object.pose.bones["leg_toon_1_L"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_panel"].layer_toon_leg_L == True)).bone_name= "leg_toon_1_L"
        
#        col_2.operator('object.toggle_prop',text="Leg R", icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_leg_R']]).sf_prop="g_leg_R"
        col_2.box().prop(context.active_object.pose.bones['properties_panel'].menu_pick,'g_leg_R', text="Leg R", toggle=True, emboss=False, icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_leg_R']])
            
        if (bpy.context.object.pose.bones['properties_panel'].menu_pick['g_leg_R'] == True):
            
            col_loc = col_2.box()
            
            if (bpy.context.object.pose.bones["properties_leg_R"].IK == 0):
                col_loc.operator('object.pick_bone',text="Leg General",depress=(bpy.context.object.pose.bones["leg_FK_R"].bone.select==True)).bone_name= "leg_FK_R"
                col_loc.operator('object.pick_bone',text="Thigh",depress=(bpy.context.object.pose.bones["thigh_FK_R"].bone.select==True)).bone_name= "thigh_FK_R"
                col_loc.operator('object.pick_bone',text="Shin",depress=(bpy.context.object.pose.bones["shin_FK_R"].bone.select==True)).bone_name= "shin_FK_R"
                col_loc.operator('object.pick_bone',text="Foot",depress=(bpy.context.object.pose.bones["foot_FK_R"].bone.select==True)).bone_name= "foot_FK_R"
            else:
                col_loc.operator('object.pick_bone',text="IK Pivot",depress=(bpy.context.object.pose.bones["foot_IK_pivot_R"].bone.select==True)).bone_name= "foot_IK_pivot_R"
                col_loc.operator('object.pick_bone',text="Thight",depress=(bpy.context.object.pose.bones["thigh_IK_R"].bone.select==True)).bone_name= "thigh_IK_R"
                col_loc.operator('object.pick_bone',text="Knee pole",depress=(bpy.context.object.pose.bones["knee_pole_R"].bone.select==True)).bone_name= "knee_pole_R"
                col_loc.operator('object.pick_bone',text="Shin",depress=(bpy.context.object.pose.bones["shin_IK_R"].bone.select==True)).bone_name= "shin_IK_R"
                col_loc.operator('object.pick_bone',text="Ankle",depress=(bpy.context.object.pose.bones["ankle_roll_R"].bone.select==True)).bone_name= "ankle_roll_R"
                col_loc.operator('object.pick_bone',text="Foot",depress=(bpy.context.object.pose.bones["foot_IK_R"].bone.select==True)).bone_name= "foot_IK_R"
            
            col_loc.operator('object.pick_bone',text="Toe",depress=(bpy.context.object.pose.bones["toe_R"].bone.select==True)).bone_name= "toe_R"
            
            col_loc.operator('object.pick_bone',text="Thigh Toon",depress=(bpy.context.object.pose.bones["leg_toon_3_R"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_panel"].layer_toon_leg_R == True)).bone_name= "leg_toon_3_R"
            col_loc.operator('object.pick_bone',text="Knee Toon",depress=(bpy.context.object.pose.bones["leg_toon_2_R"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_panel"].layer_toon_leg_R == True)).bone_name= "leg_toon_2_R"
            col_loc.operator('object.pick_bone',text="Shin Toon",depress=(bpy.context.object.pose.bones["leg_toon_1_R"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_panel"].layer_toon_leg_R == True)).bone_name= "leg_toon_1_R"
            
        col_row = col.row()
        col_1 = col_row.column(align=1)
        
#        col_1.operator('object.toggle_prop',text="Master", icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_master']]).sf_prop="g_master"
        col_1.box().prop(context.active_object.pose.bones['properties_panel'].menu_pick,'g_master', text="Master", toggle=True, emboss=False, icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_pick['g_master']])
        
        if (bpy.context.object.pose.bones['properties_panel'].menu_pick['g_master'] == True):
            
            col_loc = col_1.box()
            
            col_loc.operator('object.pick_bone',text="Master",depress=(bpy.context.object.pose.bones["master"].bone.select==True)).bone_name= "master"
            col_loc.operator('object.pick_bone',text="Virtual pivot",depress=(bpy.context.object.pose.bones["master_pivot"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_panel"].virtual_pivot_master == True)).bone_name= "master_pivot"
            col_loc.operator('object.pick_bone',text="Virtual master",depress=(bpy.context.object.pose.bones["master_pivot_target"].bone.select==True), emboss=(bpy.context.object.pose.bones["properties_panel"].virtual_pivot_master == True)).bone_name= "master_pivot_target"
            col_loc.operator('object.pick_bone',text="Root",depress=(bpy.context.object.pose.bones["root"].bone.select==True)).bone_name= "root"

#-------------------------- Registers

rpckcls = [MenuPick,OBJECT_OT_ToggleProp,OBJECT_OT_PICKER_Bones,PANEL_PT_PickerBody]

def register_Panel_Picker_Biped():
    
    for rpp in rpckcls:
        bpy.utils.register_class(rpp)
    
    PoseBone.menu_pick = PointerProperty(type=MenuPick, override={'LIBRARY_OVERRIDABLE'})
    
def unregister_Panel_Picker_Biped():
    
    for rpp in reversed(rpckcls):
        bpy.utils.unregister_class(rpp)
    
    del PoseBone.menu_pick

if __name__ == "__main__":
    register_Panel_Picker_Biped()