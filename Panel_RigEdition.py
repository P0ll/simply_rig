import bpy
from bpy.types import Operator, PropertyGroup, Scene, Menu, PoseBone
from bpy.props import BoolProperty, IntProperty, PointerProperty, StringProperty
import webbrowser

#-------------------------- ID
 
rig_id = "simply_rig"

#-------------------------- Tabs 

ui_list = ['Item','Simply rig UI'] # space avaibles to draw the panel
ui_location = ui_list[1] # this value chooses the location to run the panel

#-------------------------- Icon style

submenus = ["RIGHTARROW", "DOWNARROW_HLT"]

#-------------------------- Bones Groups
                            # write here the naming convention for your controls
                            # important! Do not change the order of the controller names, to do so will break the IK/FK snap system

arm_left = ['properties_arm_L', 'hand_FK_L', 'forearm_FK_L' , 'upperarm_FK_L', 'hand_IK_L', 'forearm_IK_L' , 'upperarm_IK_L', 'elbow_pole_L', 'arm_FK_L', 'arm_strech_length_L', 'hand_IK_pivot_L', 'arm_toon_3_L', 'arm_toon_2_L', 'arm_toon_1_L', 'shoulder_L']
arm_right = ['properties_arm_R', 'hand_FK_R', 'forearm_FK_R' , 'upperarm_FK_R', 'hand_IK_R', 'forearm_IK_R' , 'upperarm_IK_R', 'elbow_pole_R', 'arm_FK_R', 'arm_strech_length_R', 'hand_IK_pivot_R', 'arm_toon_3_R', 'arm_toon_2_R', 'arm_toon_1_R', 'shoulder_R']
leg_left = ['properties_leg_L', 'foot_FK_L', 'shin_FK_L' , 'thigh_FK_L', 'foot_IK_L', 'shin_IK_L' , 'thigh_IK_L', 'knee_pole_L', 'leg_FK_L', 'leg_strech_length_L', 'foot_IK_pivot_L', 'ankle_roll_L', 'toe_L', 'leg_toon_3_L', 'leg_toon_2_L', 'leg_toon_1_L']
leg_right = ['properties_leg_R', 'foot_FK_R', 'shin_FK_R' , 'thigh_FK_R', 'foot_IK_R', 'shin_IK_R' , 'thigh_IK_R', 'knee_pole_R', 'leg_FK_R', 'leg_strech_length_R', 'foot_IK_pivot_R', 'ankle_roll_R', 'toe_R', 'leg_toon_3_R', 'leg_toon_2_R', 'leg_toon_1_R']
hand_properties_left = ['properties_fh_thumb_L', 'properties_fh_index_L', 'properties_fh_middle_L', 'properties_fh_ring_L', 'properties_fh_pinky_L', 'fingers_IK_L', 'fingers_L']
hand_properties_right = ['properties_fh_thumb_R', 'properties_fh_index_R', 'properties_fh_middle_R', 'properties_fh_ring_R', 'properties_fh_pinky_R', 'fingers_IK_R', 'fingers_R']

hand_fingers_thumb_left = ['fh_thumb_general_L', 'fh_thumb_1_L', 'fh_thumb_2_L', 'fh_thumb_3_L', 'fh_thumb_IK_L', 'fh_thumb_deformer_L']
hand_fingers_thumb_right = ['fh_thumb_general_R', 'fh_thumb_1_R', 'fh_thumb_2_R', 'fh_thumb_3_R', 'fh_thumb_IK_R', 'fh_thumb_deformer_R']
hand_fingers_index_left = ['fh_index_general_L', 'fh_index_1_L', 'fh_index_2_L', 'fh_index_3_L', 'fh_index_4_L', 'fh_index_IK_L', 'fh_index_deformer_L']
hand_fingers_index_right = ['fh_index_general_R', 'fh_index_1_R', 'fh_index_2_R', 'fh_index_3_R', 'fh_index_4_R', 'fh_index_IK_R', 'fh_index_deformer_R']
hand_fingers_middle_left = ['fh_middle_general_L', 'fh_middle_1_L', 'fh_middle_2_L', 'fh_middle_3_L', 'fh_middle_4_L', 'fh_middle_IK_L', 'fh_middle_deformer_L']
hand_fingers_middle_right = ['fh_middle_general_R', 'fh_middle_1_R', 'fh_middle_2_R', 'fh_middle_3_R', 'fh_middle_4_R', 'fh_middle_IK_R', 'fh_middle_deformer_R']
hand_fingers_ring_left = ['fh_ring_general_L', 'fh_ring_1_L', 'fh_ring_2_L', 'fh_ring_3_L', 'fh_ring_4_L', 'fh_ring_IK_L', 'fh_ring_deformer_L']
hand_fingers_ring_right = ['fh_ring_general_R', 'fh_ring_1_R', 'fh_ring_2_R', 'fh_ring_3_R', 'fh_ring_4_R', 'fh_ring_IK_R', 'fh_ring_deformer_R']
hand_fingers_pinky_left = ['fh_pinky_general_L', 'fh_pinky_1_L', 'fh_pinky_2_L', 'fh_pinky_3_L', 'fh_pinky_4_L', 'fh_pinky_IK_L', 'fh_pinky_deformer_L']
hand_fingers_pinky_right = ['fh_pinky_general_R', 'fh_pinky_1_R', 'fh_pinky_2_R', 'fh_pinky_3_R', 'fh_pinky_4_R', 'fh_pinky_IK_R', 'fh_pinky_deformer_R']

edt_hand_fingers_ring_l = ['fh_ring_IK_L','ring_1_edit_L','ring_2_edit_L','ring_3_edit_L','ring_4_edit_L']
edt_hand_fingers_ring_r = ['fh_ring_IK_R','ring_1_edit_R','ring_2_edit_R','ring_3_edit_R','ring_4_edit_R']

def_hand_fingers_ring_l = ['fh_ring_1_L','fh_ring_2_L','fh_ring_3_L','fh_ring_4_L']
def_hand_fingers_ring_r = ['fh_ring_1_R','fh_ring_2_R','fh_ring_3_R','fh_ring_4_R']

head = ['head_rot', 'head', 'head_toon', 'neck_rot']

torso = ['chest_IK', 'chest_FK', 'spine_FK_1', 'spine_FK_2', 'spine_FK_inv_1', 'spine_FK_inv_2', 'spine_toon', 'hip_IK', 'hip_FK']

all_ctrl = [head,torso,arm_left,arm_right,leg_left,leg_right,hand_fingers_thumb_left,hand_fingers_thumb_right,hand_fingers_index_left,hand_fingers_index_right,hand_fingers_middle_left,hand_fingers_middle_right,hand_fingers_ring_left,hand_fingers_ring_right,hand_fingers_pinky_left,hand_fingers_pinky_right]

plus_ctrl = [head+torso+arm_left+arm_right+leg_left+leg_right+hand_fingers_thumb_left+hand_fingers_thumb_right+hand_fingers_index_left+hand_fingers_index_right+hand_fingers_middle_left+hand_fingers_middle_right+hand_fingers_ring_left+hand_fingers_ring_right+hand_fingers_pinky_left+hand_fingers_pinky_right]

fh_l_ctrl = [hand_fingers_thumb_left,hand_fingers_index_left,hand_fingers_middle_left,hand_fingers_ring_left,hand_fingers_pinky_left]

fh_r_ctrl = [hand_fingers_thumb_right,hand_fingers_index_right,hand_fingers_middle_right,hand_fingers_ring_right,hand_fingers_pinky_right]

#-------------------------- Functions

def is_selected(names):
    try:
        for name in names:
            if bpy.context.active_pose_bone.name == name:
                return True
        for bone in bpy.context.selected_pose_bones:
            for name in names:
                if bone.name == name:
                    return True
    except AttributeError:
        pass
    return False

def select_op(self, context, event, bone_name):
    armobj = bpy.context.active_object
    arm = bpy.context.active_object.data
    
    if (bone_name in armobj.pose.bones):
        Bone = armobj.pose.bones[bone_name]

        if event.ctrl == True or event.shift == True:
            selected = [b.name for b in bpy.context.selected_pose_bones]

            for b in armobj.pose.bones:
                b.bone.select = 0
            arm.bones.active = Bone.bone
            
            for b in armobj.pose.bones:
                if (b.name in selected):
                    b.bone.select = 1
        else:
            for b in armobj.pose.bones:
                b.bone.select = 0
            arm.bones.active = Bone.bone

#-------------------------- Operators

class OBJECT_OT_PlaceHolder(Operator):
    
    """ """
    
    bl_idname = "object.place_holder" 
    bl_label = ""
    
    def execute(self, context):
        
        return {'FINISHED'}

class OBJECT_OT_Pick_Bones(Operator):
    
    """Picking bones"""
    
    bl_idname = "object.picking_bone" 
    bl_label = "Picking bones"
    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}
    bone_name: StringProperty(name="bone_name")

    def invoke(self, context, event):
        select_op(self, context, event, self.bone_name)
        return {"FINISHED"}
    
class OBJECT_OT_TutorialReference(Operator):
    
    """web help"""
    
    bl_idname = "object.tutorial_reference" 
    bl_label = ""
    
    def execute(self, context):
        
        webbrowser.open('https://www.youtube.com/channel/UCR4Vr2PmyMyTjO7KoFi1xjQ')
        
        return {'FINISHED'}

layersbsc = [1,2,3,4,5,16,17,18,19]
prpsbsc = ["properties_fh_thumb_L","properties_fh_index_L","properties_fh_middle_L","properties_fh_ring_L","properties_fh_pinky_L","properties_fh_thumb_R","properties_fh_index_R","properties_fh_middle_R","properties_fh_ring_R","properties_fh_pinky_R"]
fngrsgrp = [hand_fingers_thumb_left, hand_fingers_thumb_right, hand_fingers_index_left, hand_fingers_index_right, hand_fingers_middle_left, hand_fingers_middle_right, hand_fingers_ring_left, hand_fingers_ring_right, hand_fingers_pinky_left, hand_fingers_pinky_right]
fngrsedt = [0,1,2,3,4,5,edt_hand_fingers_ring_l,edt_hand_fingers_ring_r,8,9]
fngrsdef = [0,1,2,3,4,5,def_hand_fingers_ring_l,def_hand_fingers_ring_r,8,9]

class OBJECT_OT_EditFingers(Operator):
    
    """activate visible number of the fingers"""
    
    bl_idname = "object.edit_fingers" 
    bl_label = "edit fingers"
    s_group: IntProperty(name = "", description="", default = 0, min = 0, max = 100)
    
    def execute(self, context):
        
        if self.s_group==6:
            for fngrpl in fngrsgrp[self.s_group]:
                bpy.context.object.data.bones[fngrpl].layers[4] = False
            for fngrpr in fngrsgrp[self.s_group+1]:
                bpy.context.object.data.bones[fngrpr].layers[5] = False
                
            for fnedtl in fngrsedt[self.s_group]:
                bpy.context.object.data.bones[fnedtl].layers[31] = False
            for fnedtr in fngrsedt[self.s_group+1]:
                bpy.context.object.data.bones[fnedtr].layers[31] = False
                
            for fndefl in fngrsdef[self.s_group]:
                bpy.context.object.data.bones[fndefl].use_deform = False
            for fndefr in fngrsdef[self.s_group+1]:
                bpy.context.object.data.bones[fndefr].use_deform = False
                
            bpy.context.object.pose.bones["properties_panel"]["rig_hand_fingers"] = 4

        if self.s_group==8:
            for fngrpl2 in fngrsgrp[self.s_group-2]:
                bpy.context.object.data.bones[fngrpl2].layers[4] = True
            for fngrpr2 in fngrsgrp[self.s_group+1-2]:
                bpy.context.object.data.bones[fngrpr2].layers[5] = True
            
            for fnedtl2 in fngrsedt[self.s_group-2]:
                bpy.context.object.data.bones[fnedtl2].layers[31] = True
            for fnedtr2 in fngrsedt[self.s_group+1-2]:
                bpy.context.object.data.bones[fnedtr2].layers[31] = True
                
            for fndefl2 in fngrsdef[self.s_group-2]:
                bpy.context.object.data.bones[fndefl2].use_deform = True
            for fndefr2 in fngrsdef[self.s_group+1-2]:
                bpy.context.object.data.bones[fndefr2].use_deform = True
                
            bpy.context.object.pose.bones["properties_panel"]["rig_hand_fingers"] = 5
        
        return {'FINISHED'} 
    
class OBJECT_OT_EditRig(Operator):
    
    """activate reproportion of the rig"""
    
    bl_idname = "object.edit_rig" 
    bl_label = "edit rig"
    
    def execute(self, context):
        
        if (bpy.context.object.pose.bones['properties_panel'].edit == False):
            
            bpy.context.object.data.layers[31] = True
            
            for edt1 in layersbsc:
                bpy.context.object.data.layers[edt1] = False

            bpy.context.object.show_in_front = True
            bpy.context.object.pose.use_mirror_x = True

            bpy.context.object.pose.bones[hand_properties_left[5]].pole_space = 1
            bpy.context.object.pose.bones[hand_properties_right[5]].pole_space = 1
            bpy.context.object.pose.bones[arm_left[0]].IK = False
            bpy.context.object.pose.bones[arm_right[0]].IK = False
            bpy.context.object.pose.bones[leg_left[0]].IK = False
            bpy.context.object.pose.bones[leg_right[0]].IK = False
            bpy.context.object.pose.bones["properties_panel"]["visibility_IKFK_arm_L"] = 1
            bpy.context.object.pose.bones["properties_panel"]["visibility_IKFK_arm_R"] = 1
            bpy.context.object.pose.bones["properties_panel"]["visibility_IKFK_leg_L"] = 1
            bpy.context.object.pose.bones["properties_panel"]["visibility_IKFK_leg_R"] = 1
            
            for edtprp1 in prpsbsc:
                bpy.context.object.pose.bones[edtprp1].IK = 1
                bpy.context.object.pose.bones[edtprp1].strech = 1
            
            bpy.context.object.pose.bones["properties_panel"].edit = True
                
        else:
            
            for edt2 in layersbsc:
                bpy.context.object.data.layers[edt2] = True

            bpy.context.object.data.layers[31] = False

            bpy.context.object.show_in_front = False
            bpy.context.object.pose.use_mirror_x = False
            
            bpy.context.object.pose.bones["properties_panel"]["visibility_IKFK_arm_L"] = 0
            bpy.context.object.pose.bones["properties_panel"]["visibility_IKFK_arm_R"] = 0
            bpy.context.object.pose.bones["properties_panel"]["visibility_IKFK_leg_L"] = 0
            bpy.context.object.pose.bones["properties_panel"]["visibility_IKFK_leg_R"] = 0
            
            bpy.context.object.pose.bones[leg_left[0]].IK = True
            bpy.context.object.pose.bones[leg_right[0]].IK = True
            
            for edtprp2 in prpsbsc:
                bpy.context.object.pose.bones[edtprp2].IK = 0

            bpy.context.object.pose.bones["properties_panel"].edit = False    
                   
        return {'FINISHED'}    
 
    
class OBJECT_OT_ApplyEditRig(Operator):
    
    """apply reproportion of the rig"""
    
    bl_idname = "object.apply_edit_rig" 
    bl_label = "edit rig"
    
    def execute(self, context):

        bpy.context.object.data.layers[31] = False
        
        for edt3 in layersbsc:
                bpy.context.object.data.layers[edt3] = True
        
        bpy.context.object.show_in_front = False
        bpy.context.object.pose.use_mirror_x = False
        
#        bpy.context.object.pose.bones["properties_panel"]["visibility_IKFK_arm_L"] = 0
#        bpy.context.object.pose.bones["properties_panel"]["visibility_IKFK_arm_R"] = 0
#        bpy.context.object.pose.bones["properties_panel"]["visibility_IKFK_leg_L"] = 0
#        bpy.context.object.pose.bones["properties_panel"]["visibility_IKFK_leg_R"] = 0 
        
        bpy.ops.pose.armature_apply(selected=False)
        bpy.context.object.pose.bones["properties_panel"].edit = False
        
        for edtprp3 in prpsbsc:
                bpy.context.object.pose.bones[edtprp3].IK = 0
        
        bpy.context.object.pose.bones[leg_left[0]].IK = True
        bpy.context.object.pose.bones[leg_right[0]].IK = True   
                   
        return {'FINISHED'}
    
tutorial_lmts=[0,5] # This value group define the numbers steps needed in the tutorial

class OBJECT_OT_TutorialSteps(Operator):
    
    """steps"""
    
    bl_idname = "object.tutorial_steps" 
    bl_label = ""
    t_step: IntProperty(name = "", description="", default = 0, min = -100, max = 100)
    
    def execute(self, context):
        
        if (bpy.context.object.data["tutorial_steps"]<tutorial_lmts[1] and self.t_step>0) or (bpy.context.object.data["tutorial_steps"]>tutorial_lmts[0] and self.t_step<0):
            bpy.context.object.data["tutorial_steps"] = bpy.context.object.data["tutorial_steps"]+self.t_step
            
        if bpy.context.object.data["tutorial_steps"]>3 and (bpy.context.object.pose.bones['properties_panel']['edit'] == True):
            bpy.ops.object.apply_edit_rig()
        
        if bpy.context.object.data["tutorial_steps"]<=2:
            bpy.context.object.data.pose_position = 'REST'
        
        if bpy.context.object.data["tutorial_steps"]==3:
            bpy.context.object.data.pose_position = 'POSE'
        
        if bpy.context.object.data["tutorial_steps"]==4:
            
            bpy.context.object.data.layers[31] = False
            
            bpy.context.object.pose.bones["properties_panel"]["visibility_IKFK_arm_L"] = 1
            bpy.context.object.pose.bones["properties_panel"]["visibility_IKFK_arm_R"] = 1
            bpy.context.object.pose.bones["properties_panel"]["visibility_IKFK_leg_L"] = 1
            bpy.context.object.pose.bones["properties_panel"]["visibility_IKFK_leg_R"] = 1
            
        if bpy.context.object.data["tutorial_steps"]==tutorial_lmts[1]:
            
            bpy.context.object.pose.bones["properties_panel"]["visibility_IKFK_arm_L"] = 0
            bpy.context.object.pose.bones["properties_panel"]["visibility_IKFK_arm_R"] = 0
            bpy.context.object.pose.bones["properties_panel"]["visibility_IKFK_leg_L"] = 0
            bpy.context.object.pose.bones["properties_panel"]["visibility_IKFK_leg_R"] = 0
            
        return {'FINISHED'} 
        
#-------------------------- Rig edition Panel

edit_rig = 1

class MenuPropScn(PropertyGroup):

    IK_double_values: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    pose_tools: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    flip: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})

class MenuPropEdit(PropertyGroup):
    
    group_head: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    
    group_arm_L: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    group_arm_R: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    
    group_torso: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    
    group_leg_L: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    group_leg_R: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    
    group_hand_L: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    group_hand_R: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    
    fh_thumb_L: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    fh_index_L: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    fh_middle_L: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    fh_ring_L: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    fh_pinky_L: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    
    fh_thumb_R: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    fh_index_R: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    fh_middle_R: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    fh_ring_R: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    fh_pinky_R: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    
class PANEL_PT_RigEdition(bpy.types.Panel):
    
    """Draw options for edit the rig"""
    
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = ui_location
    bl_label = ""
    
    def draw_header(self, context):
        self.layout.label(text='Simply rig Edition', icon="TOOL_SETTINGS")

    @classmethod
    def poll(self, context):
        if context.mode != 'POSE':
            return False

        try:
           ob = context.active_object
           return (context.active_object.data.get("rig_id") == rig_id) and context.active_object.data.get("edit_rig") == edit_rig
        except AttributeError:
           return 0   
       
    def draw(self, context):
        layout = self.layout
        col = layout.column()
        
        t_titles=["Simply rig introduction","Character name","Fingers count","Reproportion mode","Relocate boneshapes","Rig finished","",""]
        arrows=["BLANK1","BACK","FORWARD"]
         
        row = col.row(align=True)
        row.scale_y=1.5
        row.scale_x=3
        row.operator('object.tutorial_steps',text="", icon=arrows[bpy.context.object.data["tutorial_steps"]>tutorial_lmts[0]], emboss=(bpy.context.object.data["tutorial_steps"]>tutorial_lmts[0])).t_step=-1
        row.operator('object.place_holder',text = t_titles[bpy.context.object.data["tutorial_steps"]], emboss=False, depress=True)
        row.scale_x=3
        row.operator('object.tutorial_steps',text="", icon=arrows[(bpy.context.object.data["tutorial_steps"]<tutorial_lmts[1])*2], emboss=(bpy.context.object.data["tutorial_steps"]<tutorial_lmts[1])).t_step=1
        row = col.row()
        row.separator()
        
        if bpy.context.object.data["tutorial_steps"]==0:
            
            row = col.row()
            row.alignment = 'CENTER'
            row.label(text = "Welcome to Simply rig configuration assistant,")
            row = col.row()
            row.alignment = 'CENTER'
            row.label(text = "to begin rig your character press the arrow")
        
        if bpy.context.object.data["tutorial_steps"]==tutorial_lmts[1]:
            
            row = col.row()
            row.alignment = 'CENTER'
            row.label(text = "Nice work, rig setup is done,")
            row = col.row()
            row.alignment = 'CENTER'
            row.label(text = "for disable the assistant put 0")
            row = col.row()
            row.alignment = 'CENTER'
            row.prop(bpy.context.object.data,'["edit_rig"]', text="")
        
        if bpy.context.object.data["tutorial_steps"]==1:
            
            row = col.row(align=True)
            row.alignment = 'CENTER'
            row.prop(bpy.context.active_object.pose.bones['properties_panel'],'["character_name"]', text="")
                    
        if bpy.context.object.data["tutorial_steps"]==2:
                    
            row = col.row(align=True)
            
            row.operator("object.edit_fingers",text="4 Fingers", icon="VIEW_PAN", depress=(bpy.context.object.pose.bones["properties_panel"]["rig_hand_fingers"]==4)).s_group=6
            row.operator("object.edit_fingers",text="5 Fingers", icon="VIEW_PAN", depress=(bpy.context.object.pose.bones["properties_panel"]["rig_hand_fingers"]==5)).s_group=8
        
        if bpy.context.object.data["tutorial_steps"]==3:
            
            row = col.row(align=True)
            row.operator('object.edit_rig',text="Enable", depress = (bpy.context.object.pose.bones['properties_panel']["edit"]==True))
            row.operator('object.edit_rig',text="Disable", depress = (bpy.context.object.pose.bones['properties_panel']["edit"]==False))
        
        if bpy.context.object.data["tutorial_steps"]==4:
            
            row = col.row()
            
            box = layout.row()
            col = box.column()
            
            col_row = col.row()
            
            col_1 = col_row.column(align = 0)
            col_2 = col_row.column(align = 0)
            
            col_loc = col_2
            
            grprp1 = ["Head","Arm L","Arm R","Torso","Leg L","Leg R"]
            grprp2 = ['group_head','group_arm_L','group_arm_R','group_torso','group_leg_L','group_leg_R']
            grprp3 = [head,arm_left,arm_right,torso,leg_left,leg_right]
            
            grseq=[0,1,2,3,4,5]
            
            for gseq in grseq:
            
                col_loc.prop(context.active_object.pose.bones['properties_panel'].menu_edit,grprp2[gseq], text=grprp1[gseq], toggle=True, emboss=False, icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_edit[grprp2[gseq]]])
                
                if (bpy.context.object.pose.bones['properties_panel'].menu_edit[grprp2[gseq]] == True):
                    
                    col_box=col_loc.box()
                    
                    for hdea in grprp3[gseq]:
                                            
                        col_box.operator('object.picking_bone',text=hdea,depress=(bpy.context.object.pose.bones[hdea].bone.select==True)).bone_name= hdea
            
            fhprp1 = ["Thumb","Index","Middle","Ring","Pinky"]
            
            fhlprp2 = ['fh_thumb_L','fh_index_L','fh_middle_L','fh_ring_L','fh_pinky_L']
            fhlprp3 = [hand_fingers_thumb_left,hand_fingers_index_left,hand_fingers_middle_left,hand_fingers_ring_left,hand_fingers_pinky_left]
            
            fhrprp2 = ['fh_thumb_R','fh_index_R','fh_middle_R','fh_ring_R','fh_pinky_R']
            fhrprp3 = [hand_fingers_thumb_right,hand_fingers_index_right,hand_fingers_middle_right,hand_fingers_ring_right,hand_fingers_pinky_right]
            
            if bpy.context.object.pose.bones['properties_panel']["rig_hand_fingers"] == 5:
                fhdrw=[0,1,2,3,4]
            else:
                fhdrw=[0,1,2,4]
            
            col_loc.prop(context.active_object.pose.bones['properties_panel'].menu_edit,'group_hand_L', text="Hand L", toggle=True, emboss=False, icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_edit['group_hand_L']])
            
            if (bpy.context.object.pose.bones['properties_panel'].menu_edit['group_hand_L'] == True):
                
                col_box=col_loc.box()
                
                for fhdr in fhdrw:
            
                    col_box.prop(context.active_object.pose.bones['properties_panel'].menu_edit,fhlprp2[fhdr], text=fhprp1[fhdr], toggle=True, emboss=False, icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_edit[fhlprp2[fhdr]]])
                    
                    if (bpy.context.object.pose.bones['properties_panel'].menu_edit[fhlprp2[fhdr]] == True):
                        
                        col_bo=col_box.box()
            
                        for fhlc in fhlprp3[fhdr]:
                                                
                            col_bo.operator('object.picking_bone',text=fhlc,depress=(bpy.context.object.pose.bones[fhlc].bone.select==True)).bone_name= fhlc
            
            col_loc.prop(context.active_object.pose.bones['properties_panel'].menu_edit,'group_hand_R', text="Hand R", toggle=True, emboss=False, icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_edit['group_hand_R']])
            
            if (bpy.context.object.pose.bones['properties_panel'].menu_edit['group_hand_R'] == True):
                
                col_box=col_loc.box()
            
                for fhdr in fhdrw:
            
                    col_box.prop(context.active_object.pose.bones['properties_panel'].menu_edit,fhrprp2[fhdr], text=fhprp1[fhdr], toggle=True, emboss=False, icon=submenus[bpy.context.object.pose.bones['properties_panel'].menu_edit[fhrprp2[fhdr]]])
                    
                    if (bpy.context.object.pose.bones['properties_panel'].menu_edit[fhrprp2[fhdr]] == True):
                        
                        col_bo=col_box.box()
                        
                        for fhrc in fhrprp3[fhdr]:
                                                
                            col_bo.operator('object.picking_bone',text=fhrc,depress=(bpy.context.object.pose.bones[fhrc].bone.select==True)).bone_name= fhrc
            
            col_loc = col_1
            
            if is_selected(plus_ctrl[0]):
                col_loc.label(text="Bone shape:")
            else:
                col_loc.label(text = "")
            
            allfr=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
                 
            for allctl in allfr:
            
                for allctrl in all_ctrl[allctl]:
                
                    if bpy.context.active_pose_bone.name == allctrl:
                        
                        col_loc.prop(bpy.context.object.pose.bones[allctrl],["custom_shape"][0], text="")
                        
                        col_loc.prop(bpy.context.object.pose.bones[allctrl],["custom_shape_translation"][0], text="Location")

                        col_loc.prop(bpy.context.object.pose.bones[allctrl],["custom_shape_rotation_euler"][0], text="Rotation")
                    
                        col_loc.prop(bpy.context.object.pose.bones[allctrl],["custom_shape_scale_xyz"][0], text="Scale")
            
        row = col.row()
        row.separator()
        row = col.row()
        
            
#-------------------------- Registers
 
classesedt = [MenuPropScn, MenuPropEdit, OBJECT_OT_PlaceHolder, OBJECT_OT_Pick_Bones, OBJECT_OT_TutorialReference, OBJECT_OT_EditFingers, OBJECT_OT_EditRig, OBJECT_OT_ApplyEditRig, OBJECT_OT_TutorialSteps, PANEL_PT_RigEdition]

def register_Panel_RigEdition():

    for rpre in classesedt:    
        bpy.utils.register_class(rpre)
    
    Scene.menu_propscn = PointerProperty(type=MenuPropScn)
    PoseBone.menu_edit = PointerProperty(type=MenuPropEdit, override={'LIBRARY_OVERRIDABLE'})
    
def unregister_Panel_RigEdition():
    
    for rpre in reversed(classesedt):    
        bpy.utils.unregister_class(rpre)
    
    del Scene.menu_propscn
    del PoseBone.menu_edit

if __name__ == "__main__":
    register_Panel_RigEdition()
