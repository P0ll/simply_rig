3# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
#
# ##### ACKNOWLEDGEMENTS #####
#
# Special thanks for feedback and ideas to: Sav Martin, Juan Pablo Bouza, Urko Marduit and Vicente Tortosa Mederos
#
# #########################################################################################################

import bpy
from bpy.types import Panel, Menu, Operator, PropertyGroup, AddonPreferences, PoseBone, Scene
from bpy.props import StringProperty, BoolProperty, IntProperty, FloatProperty, FloatVectorProperty, EnumProperty, PointerProperty
from bpy.utils import register_class, unregister_class

IK_values = [bpy.props.FloatProperty(name="IK", default=1.0, min=0.0, max = 1.0, override={'LIBRARY_OVERRIDABLE'}), bpy.props.BoolProperty(name="IK", default = True, override={'LIBRARY_OVERRIDABLE'})]
IK_bool = IK_values[1] # this value is to decide if IK's are floats [0] or booleans [1]

bl_info = {
    'name': 'Simply rig',
    'author': 'Poll',
    'version': (1,0,0),
    'blender': (2, 92, 0),
    'location': 'View3d tools panel, Armature Add menu',
    'description': 'Simply rig rigging system',
    'wiki_url': '',
    'tracker_url': '',
    'category': 'Rigging'}

    
if "bpy" in locals():
    import importlib
    if "Panel_RigEdition" in locals():
        importlib.reload(Panel_RigEdition)
    if "Panel_Display_Biped" in locals():
        importlib.reload(Panel_Display_Biped)
    if "Panel_Properties_Biped" in locals():
        importlib.reload(Panel_Properties_Biped)
    if "Panel_Picker_Biped" in locals():
        importlib.reload(Panel_Picker_Biped)
    if "Add_Rig" in locals():
        importlib.reload(Add_Rig)
        
class SimplyRigAddonPreferences(AddonPreferences):

    bl_idname = __name__

    def draw(self, context):
        layout = self.layout
        layout.prop(bpy.context.scene.menu_propscn,'IK_double_values', text=" Show FK status")
        layout.prop(bpy.context.scene.menu_propscn,'pose_tools', text=" Pose tools")
        layout.prop(bpy.context.scene.menu_propscn,'flip', text=" Flip order the Left and Right columns")
           
opcls = [SimplyRigAddonPreferences]

def register():
    
    for unrgt in opcls:
        register_class(unrgt)

    from .Panel_RigEdition import register_Panel_RigEdition
    register_Panel_RigEdition()
    
    from .Panel_Display_Biped import register_Panel_Display_Biped
    register_Panel_Display_Biped()
    
    from .Panel_Properties_Biped import register_Panel_Properties_Biped
    register_Panel_Properties_Biped()
    
    from .Panel_Picker_Biped import register_Panel_Picker_Biped
    register_Panel_Picker_Biped()

    from .Add_Rig import register_Add_Rig
    register_Add_Rig()
    
    PoseBone.edit = BoolProperty(name="edit", default = False, override={'LIBRARY_OVERRIDABLE'})

    PoseBone.visibility_IKFK_arm_L = BoolProperty(name="visibility_IKFK_arm_L", default = False, override={'LIBRARY_OVERRIDABLE'})
    PoseBone.visibility_IKFK_arm_R = BoolProperty(name="visibility_IKFK_arm_R", default = False, override={'LIBRARY_OVERRIDABLE'})
    PoseBone.visibility_IKFK_leg_L = BoolProperty(name="visibility_IKFK_leg_L", default = False, override={'LIBRARY_OVERRIDABLE'})
    PoseBone.visibility_IKFK_leg_R = BoolProperty(name="visibility_IKFK_leg_R", default = False, override={'LIBRARY_OVERRIDABLE'})

    PoseBone.rig_scale = FloatProperty(name="rig_scale", default=1.000, min=0.001, max = 1000.000, override={'LIBRARY_OVERRIDABLE'})
    
    PoseBone.layer_toon_arm_L = BoolProperty(description = 'toon controls', name="layer_toon_arm_L", default = False, override={'LIBRARY_OVERRIDABLE'})
    PoseBone.layer_toon_arm_R = BoolProperty(description = 'toon controls', name="layer_toon_arm_R", default = False, override={'LIBRARY_OVERRIDABLE'})
    PoseBone.layer_toon_leg_L = BoolProperty(description = 'toon controls', name="layer_toon_leg_L", default = False, override={'LIBRARY_OVERRIDABLE'})
    PoseBone.layer_toon_leg_R = BoolProperty(description = 'toon controls', name="layer_toon_leg_R", default = False, override={'LIBRARY_OVERRIDABLE'})

    PoseBone.mesh_visibility_head = BoolProperty(description = '', name="mesh_visibility_head", default = False, override={'LIBRARY_OVERRIDABLE'})
    PoseBone.mesh_visibility_torso = BoolProperty(description = '', name="mesh_visibility_torso", default = False, override={'LIBRARY_OVERRIDABLE'})
    PoseBone.mesh_visibility_arm_L = BoolProperty(description = '', name="mesh_visibility_arm_L", default = False, override={'LIBRARY_OVERRIDABLE'})
    PoseBone.mesh_visibility_arm_R = BoolProperty(description = '', name="mesh_visibility_arm_R", default = False, override={'LIBRARY_OVERRIDABLE'})
    PoseBone.mesh_visibility_hand_L = BoolProperty(description = '', name="mesh_visibility_hand_L", default = False, override={'LIBRARY_OVERRIDABLE'})
    PoseBone.mesh_visibility_hand_R = BoolProperty(description = '', name="mesh_visibility_hand_R", default = False, override={'LIBRARY_OVERRIDABLE'})
    PoseBone.mesh_visibility_leg_L = BoolProperty(description = '', name="mesh_visibility_leg_L", default = False, override={'LIBRARY_OVERRIDABLE'})
    PoseBone.mesh_visibility_leg_R = BoolProperty(description = '', name="mesh_visibility_leg_R", default = False, override={'LIBRARY_OVERRIDABLE'})
    
    PoseBone.IK = IK_bool
    PoseBone.strech = FloatProperty(name="strech", default=1.0, min=0.0, max = 1.0, override={'LIBRARY_OVERRIDABLE'})
    PoseBone.no_pole = BoolProperty(description = 'Disable pole to orientate articulation', name="no_pole", default = False, override={'LIBRARY_OVERRIDABLE'})
    PoseBone.pole_lock = BoolProperty(name="pole_lock", default = False, override={'LIBRARY_OVERRIDABLE'})
    PoseBone.pole_space = IntProperty(name="pole_space", default=1, min=0, max = 2, override={'LIBRARY_OVERRIDABLE'})
    
    PoseBone.keyframe_arms = BoolProperty(description = '', name="keyframe_arms", default = False, override={'LIBRARY_OVERRIDABLE'})
    PoseBone.keyframe_legs = BoolProperty(description = '', name="keyframe_legs", default = False, override={'LIBRARY_OVERRIDABLE'})
      
    PoseBone.virtual_pivot_torso = BoolProperty(description = '', name="virtual_pivot_torso", default = False, override={'LIBRARY_OVERRIDABLE'})
    PoseBone.virtual_pivot_master = BoolProperty(description = '', name="virtual_pivot_master", default = False, override={'LIBRARY_OVERRIDABLE'})
     
    PoseBone.no_rot = BoolProperty(name="no_rot", default = False, override={'LIBRARY_OVERRIDABLE'})
    PoseBone.roll_limit = FloatProperty(name="roll_limit", default=1.0, min=0.0, max = 180.0, override={'LIBRARY_OVERRIDABLE'})
    
def unregister():
    
    for unrgt in reversed(opcls):
        unregister_class(unrgt)

    from .Panel_RigEdition import unregister_Panel_RigEdition
    unregister_Panel_RigEdition()    
    
    from .Panel_Display_Biped import unregister_Panel_Display_Biped
    unregister_Panel_Display_Biped()
    
    from .Panel_Properties_Biped import unregister_Panel_Properties_Biped
    unregister_Panel_Properties_Biped()
    
    from .Panel_Picker_Biped import unregister_Panel_Picker_Biped
    unregister_Panel_Picker_Biped()

    from .Add_Rig import unregister_Add_Rig
    unregister_Add_Rig()
    
    del PoseBone.edit

    del PoseBone.visibility_IKFK_arm_L
    del PoseBone.visibility_IKFK_arm_R
    del PoseBone.visibility_IKFK_leg_L
    del PoseBone.visibility_IKFK_leg_R

    del PoseBone.rig_scale
    
    del PoseBone.layer_toon_arm_L
    del PoseBone.layer_toon_arm_R
    del PoseBone.layer_toon_leg_L
    del PoseBone.layer_toon_leg_R

    del PoseBone.mesh_visibility_head
    del PoseBone.mesh_visibility_torso
    del PoseBone.mesh_visibility_arm_L
    del PoseBone.mesh_visibility_arm_R
    del PoseBone.mesh_visibility_hand_L
    del PoseBone.mesh_visibility_hand_R
    del PoseBone.mesh_visibility_leg_L
    del PoseBone.mesh_visibility_leg_R

    del PoseBone.IK
    del PoseBone.strech
    del PoseBone.no_pole
    del PoseBone.pole_lock
    del PoseBone.pole_space
    
    del PoseBone.keyframe_arms
    del PoseBone.keyframe_legs
    
    del PoseBone.virtual_pivot_torso
    del PoseBone.virtual_pivot_master
    
    del PoseBone.no_rot
    del PoseBone.roll_limit