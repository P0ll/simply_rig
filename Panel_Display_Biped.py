import bpy
from bpy.types import Panel, Operator, PropertyGroup, PoseBone, Scene
from bpy.props import StringProperty, BoolProperty, IntProperty, FloatProperty, FloatVectorProperty, EnumProperty, PointerProperty
from bpy.utils import register_class, unregister_class

#-------------------------- ID
 
rig_id = "simply_rig"
rig_type = "biped"
project_id = "generic"

#-------------------------- Tabs 

ui_list = ['Item', 'Simply rig UI'] # space avaibles to draw the panel
ui_location = ui_list[1] # this value chooses the location to run the panel

#-------------------------- Flip Groups
                            # important no change the order for the (1º)left and (2º)right groups

eyes_icon = ["HIDE_ON", "HIDE_OFF"]                                                        
icon_flip = ['EVENT_L','EVENT_R']
layers_flip = ['layer_toon_arm_L', 'layer_toon_arm_R', 'layer_toon_leg_L', 'layer_toon_leg_R']

#-------------------------- Panel Properties
    
class MenuDsply(PropertyGroup):
    
    menu_layers: BoolProperty(name="", description="", default = True, override={'LIBRARY_OVERRIDABLE'})
    
    sync_mesh_bones_head: BoolProperty(description = '', name="", default = False, override={'LIBRARY_OVERRIDABLE'})
    sync_mesh_bones_torso: BoolProperty(description = '', name="", default = False, override={'LIBRARY_OVERRIDABLE'})
    sync_mesh_bones_arm_L: BoolProperty(description = '', name="", default = False, override={'LIBRARY_OVERRIDABLE'})
    sync_mesh_bones_arm_R: BoolProperty(description = '', name="", default = False, override={'LIBRARY_OVERRIDABLE'})
    sync_mesh_bones_hand_L: BoolProperty(description = '', name="", default = False, override={'LIBRARY_OVERRIDABLE'})
    sync_mesh_bones_hand_R: BoolProperty(description = '', name="", default = False, override={'LIBRARY_OVERRIDABLE'})
    sync_mesh_bones_leg_L: BoolProperty(description = '', name="", default = False, override={'LIBRARY_OVERRIDABLE'})
    sync_mesh_bones_leg_R: BoolProperty(description = '', name="", default = False, override={'LIBRARY_OVERRIDABLE'})

#-------------------------- Display UI 

class PANEL_PT_RigDisplay(Panel):
    
    """Draw panels for display controls"""
    
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = ui_location
    bl_label = ""
#    bl_options = {'DRAW_BOX'}
    
    def draw_header(self, context):
        self.layout.label(text=bpy.data.objects[context.active_object.name].pose.bones["properties_panel"]["character_name"] + " Display", icon="RESTRICT_VIEW_OFF")

    @classmethod
    def poll(self, context):
        if context.mode != 'POSE':
            return False

        try:
           ob = context.active_object
           return (context.active_object.data.get("rig_id") == rig_id) and (context.active_object.data.get("rig_type") == rig_type)  and (context.active_object.data.get("project_id") == project_id)
        except AttributeError:
           return 0
       
    def draw(self, context):
        flip = bpy.context.scene.menu_propscn.flip
        layout = self.layout
        col = layout.column()  
         
        row = col.row(align = 1)
        row.prop(context.active_object.pose.bones['properties_panel'].menu_dsply,'menu_layers', text="Controls layers", icon="GROUP_BONE", toggle=True)
        row.prop(context.active_object.pose.bones['properties_panel'].menu_dsply,'menu_layers', text="Body parts visibility", icon="HIDE_OFF", toggle=True, invert_checkbox = True)
            
        row = col.row()
        
        box = layout.row()
        col = box.column()
        box = col.box()
        
        if (context.object.pose.bones['properties_panel'].menu_dsply.menu_layers == True):
              
            row = box.row()
            
            row.prop(context.active_object.data, "layers", index=1, icon="USER", toggle=True, text="Head")
    
            col_row = box.row()
            col_1 = col_row.column(align = 1)
            col_2 = col_row.column(align = 1)
            
            col_1 = col_1.row(align = 1)
            col_1.scale_x=2.8
            col_1.prop(context.active_object.data, "layers", icon=icon_flip[0+flip], index=2+flip, toggle=True, text="Arm")
            
            col_1.scale_x=1
            col_1.prop(context.active_object.pose.bones['properties_panel'],layers_flip[0+flip], toggle=True, text="Toon", emboss = (context.object.data.layers[2+flip] == True))
                      
            col_2 = col_2.row(align = 1)
            col_2.scale_x=2.8
            col_2.prop(context.active_object.data, "layers", icon=icon_flip[1-flip], index=3-flip, toggle=True, text="Arm")
            
            col_2.scale_x=1 
            col_2.prop(context.active_object.pose.bones['properties_panel'],layers_flip[1-flip], toggle=True, text="Toon", emboss = (context.object.data.layers[3-flip] == True))
            
            row = box.row()

            row.prop(context.active_object.data, "layers", icon=icon_flip[0+flip], index=4+flip, toggle=True, text="Fingers")
            row.prop(context.active_object.data, "layers", icon=icon_flip[1-flip], index=5-flip, toggle=True, text="Fingers")
            
            col_row = box.row()
            col_1 = col_row.column(align = 1)
            
            col_1 = col_1.row(align = 1) 
            col_1.scale_x=3
            col_1.prop(context.active_object.data, "layers", icon="OUTLINER_OB_ARMATURE", index=17, toggle=True, text="Torso")
            
            col_1.scale_x=1
            col_1.prop(context.active_object.pose.bones['properties_panel'],'virtual_pivot_torso', toggle=True, text="V.Pivot", emboss = (context.object.data.layers[17] == True))   
            
            col_row = box.row()
            col_1 = col_row.column(align = 1)
            col_2 = col_row.column(align = 1)
            
            col_1 = col_1.row(align = 1)
            col_1.scale_x=2.8
            col_1.prop(context.active_object.data, "layers", icon=icon_flip[0+flip], index=18+flip, toggle=True, text="Leg")
            
            col_1.scale_x=1
            col_1.prop(context.active_object.pose.bones['properties_panel'],layers_flip[2+flip], toggle=True, text="Toon", emboss = (context.object.data.layers[18+flip] == True))
            
            col_2 = col_2.row(align = 1)
            col_2.scale_x=2.8
            col_2.prop(context.active_object.data, "layers", icon=icon_flip[1-flip], index=19-flip, toggle=True, text="Leg")
            
            col_2.scale_x=1
            col_2.prop(context.active_object.pose.bones['properties_panel'],layers_flip[3-flip], toggle=True, text="Toon", emboss = (context.object.data.layers[19-flip] == True))
            
            col_row = box.row()
            col_1 = col_row.column(align = 1)
            
            col_1 = col_1.row(align = 1) 
            col_1.scale_x=3
            col_1.prop(context.active_object.data, "layers", icon="PIVOT_BOUNDBOX", index=16, toggle=True, text="Masters")
            
            col_1.scale_x=1
            col_1.prop(context.active_object.pose.bones['properties_panel'],'virtual_pivot_master', toggle=True, text="V.Pivot", emboss = (context.object.data.layers[16] == True))
            
            row = box.row() 
            
            row.prop(context.active_object.data, "layers", index=0, toggle=True, icon="PREFERENCES", text="Bones properties")
            
        else:            
            row = box.row(align=True) 
            
            row.label(text="", icon=eyes_icon[context.object.pose.bones['properties_panel']['mesh_visibility_head']])
            row.prop(context.active_object.pose.bones['properties_panel'].menu_dsply,'sync_mesh_bones_head', toggle=True, text="", icon="FILE_REFRESH", emboss = True)
            row.prop(context.active_object.pose.bones['properties_panel'],'mesh_visibility_head', toggle=True, text="Head", icon="USER", emboss = True)
            if (context.object.pose.bones['properties_panel']['mesh_visibility_head'] == True) and (context.object.pose.bones['properties_panel'].menu_dsply.sync_mesh_bones_head == True) and (context.object.data.layers[1] == False):
                context.object.data.layers[1] = True
            if (context.object.pose.bones['properties_panel']['mesh_visibility_head'] == False) and (context.object.pose.bones['properties_panel'].menu_dsply.sync_mesh_bones_head == True) and (context.object.data.layers[1] == True):
                context.object.data.layers[1] = False
            
            row = box.row(align=True)
            
            sync_vsb = ['sync_mesh_bones_arm_L','sync_mesh_bones_arm_R','sync_mesh_bones_hand_L','sync_mesh_bones_hand_R','sync_mesh_bones_leg_L','sync_mesh_bones_leg_R']
            msh_vsb = ['mesh_visibility_arm_L','mesh_visibility_arm_R','mesh_visibility_hand_L','mesh_visibility_hand_R','mesh_visibility_leg_L','mesh_visibility_leg_R']
            
            row.label(text="", icon=eyes_icon[context.object.pose.bones['properties_panel'][msh_vsb[0+flip]]])
            row.prop(context.active_object.pose.bones['properties_panel'].menu_dsply,sync_vsb[0+flip], toggle=True, text="", icon="FILE_REFRESH", emboss = True)
            row.prop(context.active_object.pose.bones['properties_panel'],msh_vsb[0+flip], toggle=True, text= "Arm", icon=icon_flip[0+flip], emboss = True)
            if (context.object.pose.bones['properties_panel'][msh_vsb[0+flip]] == True) and (context.object.pose.bones['properties_panel'].menu_dsply[sync_vsb[0+flip]] == True) and (context.object.data.layers[2+flip] == False):
                context.object.data.layers[2+flip] = True
            if (context.object.pose.bones['properties_panel'][msh_vsb[0+flip]] == False) and (context.object.pose.bones['properties_panel'].menu_dsply[sync_vsb[0+flip]] == True) and (context.object.data.layers[2+flip] == True):
                context.object.data.layers[2+flip] = False
            
            row.label(text="", icon=eyes_icon[context.object.pose.bones['properties_panel'][msh_vsb[1-flip]]])
            row.prop(context.active_object.pose.bones['properties_panel'].menu_dsply,sync_vsb[1-flip], toggle=True, text="", icon="FILE_REFRESH", emboss = True)
            row.prop(context.active_object.pose.bones['properties_panel'],msh_vsb[1-flip], toggle=True, text= "Arm", icon=icon_flip[1-flip], emboss = True)
            if (context.object.pose.bones['properties_panel'][msh_vsb[1-flip]] == True) and (context.object.pose.bones['properties_panel'].menu_dsply[sync_vsb[1-flip]] == True) and (context.object.data.layers[3-flip] == False):
                context.object.data.layers[3-flip] = True
            if (context.object.pose.bones['properties_panel'][msh_vsb[1-flip]] == False) and (context.object.pose.bones['properties_panel'].menu_dsply[sync_vsb[1-flip]] == True) and (context.object.data.layers[3-flip] == True):
                context.object.data.layers[3-flip] = False  
                                
            row = box.row(align=True)
            
            row.label(text="", icon=eyes_icon[context.object.pose.bones['properties_panel'][msh_vsb[2+flip]]])
            row.prop(context.active_object.pose.bones['properties_panel'].menu_dsply,sync_vsb[2+flip], toggle=True, text="", icon="FILE_REFRESH", emboss = True)
            row.prop(context.active_object.pose.bones['properties_panel'],msh_vsb[2+flip], toggle=True, text= "Fingers", icon=icon_flip[0+flip], emboss = True)
            if (context.object.pose.bones['properties_panel'][msh_vsb[2+flip]] == True) and (context.object.pose.bones['properties_panel'].menu_dsply[sync_vsb[2+flip]] == True) and (context.object.data.layers[4+flip] == False):
                context.object.data.layers[4+flip] = True
            if (context.object.pose.bones['properties_panel'][msh_vsb[2+flip]] == False) and (context.object.pose.bones['properties_panel'].menu_dsply[sync_vsb[2+flip]] == True) and (context.object.data.layers[4+flip] == True):
                context.object.data.layers[4+flip] = False
            
            row.label(text="", icon=eyes_icon[context.object.pose.bones['properties_panel'][msh_vsb[3-flip]]])
            row.prop(context.active_object.pose.bones['properties_panel'].menu_dsply,sync_vsb[3-flip], toggle=True, text="", icon="FILE_REFRESH", emboss = True)
            row.prop(context.active_object.pose.bones['properties_panel'],msh_vsb[3-flip], toggle=True, text= "Fingers", icon=icon_flip[1-flip], emboss = True)
            if (context.object.pose.bones['properties_panel'][msh_vsb[3-flip]] == True) and (context.object.pose.bones['properties_panel'].menu_dsply[sync_vsb[3-flip]] == True) and (context.object.data.layers[5-flip] == False):
                context.object.data.layers[5-flip] = True
            if (context.object.pose.bones['properties_panel'][msh_vsb[3-flip]] == False) and (context.object.pose.bones['properties_panel'].menu_dsply[sync_vsb[3-flip]] == True) and (context.object.data.layers[5-flip] == True):
                context.object.data.layers[5-flip] = False 
                          
            row = box.row(align=True)
            
            row.label(text="", icon=eyes_icon[context.object.pose.bones['properties_panel']['mesh_visibility_torso']])
            row.prop(context.active_object.pose.bones['properties_panel'].menu_dsply,'sync_mesh_bones_torso', toggle=True, text="", icon="FILE_REFRESH", emboss = True)
            row.prop(context.active_object.pose.bones['properties_panel'],'mesh_visibility_torso', toggle=True, text="Torso", icon="OUTLINER_OB_ARMATURE", emboss = True)
            if (context.object.pose.bones['properties_panel'].mesh_visibility_torso == True) and (context.object.pose.bones['properties_panel'].menu_dsply['sync_mesh_bones_torso'] == True) and (context.object.data.layers[17] == False):
                context.object.data.layers[17] = True
            if (context.object.pose.bones['properties_panel'].mesh_visibility_torso == False) and (context.object.pose.bones['properties_panel'].menu_dsply['sync_mesh_bones_torso'] == True) and (context.object.data.layers[17] == True):
                context.object.data.layers[17] = False
            
            row = box.row(align=True)
            
            row.label(text="", icon=eyes_icon[context.object.pose.bones['properties_panel'][msh_vsb[4+flip]]])
            row.prop(context.active_object.pose.bones['properties_panel'].menu_dsply,sync_vsb[4+flip], toggle=True, text="", icon="FILE_REFRESH", emboss = True)
            row.prop(context.active_object.pose.bones['properties_panel'],msh_vsb[4+flip], toggle=True, text= "Leg", icon=icon_flip[0+flip], emboss = True)
            if (context.object.pose.bones['properties_panel'][msh_vsb[4+flip]] == True) and (context.object.pose.bones['properties_panel'].menu_dsply[sync_vsb[4+flip]] == True) and (context.object.data.layers[18+flip] == False):
                context.object.data.layers[18+flip] = True
            if (context.object.pose.bones['properties_panel'][msh_vsb[4+flip]] == False) and (context.object.pose.bones['properties_panel'].menu_dsply[sync_vsb[4+flip]] == True) and (context.object.data.layers[18+flip] == True):
                context.object.data.layers[18+flip] = False
            
            row.label(text="", icon=eyes_icon[context.object.pose.bones['properties_panel'][msh_vsb[5-flip]]])
            row.prop(context.active_object.pose.bones['properties_panel'].menu_dsply,sync_vsb[5-flip], toggle=True, text="", icon="FILE_REFRESH", emboss = True)
            row.prop(context.active_object.pose.bones['properties_panel'],msh_vsb[5-flip], toggle=True, text= "Leg", icon=icon_flip[1-flip], emboss = True)
            if (context.object.pose.bones['properties_panel'][msh_vsb[5-flip]] == True) and (context.object.pose.bones['properties_panel'].menu_dsply[sync_vsb[5-flip]] == True) and (context.object.data.layers[19-flip] == False):
                context.object.data.layers[19-flip] = True
            if (context.object.pose.bones['properties_panel'][msh_vsb[5-flip]] == False) and (context.object.pose.bones['properties_panel'].menu_dsply[sync_vsb[5-flip]] == True) and (context.object.data.layers[19-flip] == True):
                context.object.data.layers[19-flip] = False

        row = col.row()
        row.separator()
        row = col.row()
        
        row.prop(context.active_object.pose.bones['properties_panel'],'rig_scale', text='scale')
        
        row = col.row()
        row.separator()
        row = col.row()
        
        row = col.row()
        row.alignment = 'LEFT'
        row.scale_x=3
        row.prop(context.object,"show_in_front", text='In front')
        row.prop(context.space_data.overlay, "show_bones", text='Controls')
        #row.prop(context.space_data.overlay, "show_overlays", text='Overlays')
        row.prop(context.scene.render, "use_simplify",text="Simplify")
        row.scale_x=0.5
        if context.scene.render.use_simplify:
            row.prop(context.scene.render, "simplify_subdivision", text="")

            
#-------------------------- Registers

classesd = [MenuDsply, PANEL_PT_RigDisplay]

def register_Panel_Display_Biped():
    
    for rpdb in classesd:
        register_class(rpdb)
    
    PoseBone.menu_dsply= PointerProperty(type=MenuDsply, override={'LIBRARY_OVERRIDABLE'})
    
def unregister_Panel_Display_Biped():

    for rpdb in reversed(classesd):
        unregister_class(rpdb)

    del PoseBone.menu_dsply
    
if __name__ == "__main__":
    register_Panel_Display_Biped()
