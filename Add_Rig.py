import bpy
import os

class Operator_Simply_Add(bpy.types.Operator):

    bl_idname = "simplyrig.add_rig"
    bl_label = "simply Rig"
    bl_description = "Generates simply rig"
    bl_options = {'REGISTER', 'UNDO',}


    @classmethod
    def poll(cls, context):            
        return bpy.context.scene != None

    def import_simply(self, context):
        CURRENT_DIR = os.path.dirname(__file__)
        filepath =  os.path.join(CURRENT_DIR, "simply_rig_biped.blend")
        scene = bpy.context.scene

        with bpy.data.libraries.load(filepath, link=False) as (data_from, data_to):
            data_to.collections = ["rig"]
            
        for collection in data_to.collections:
            scene.collection.children.link(collection)


    def execute(self, context):
        self.import_simply(context)
        return{'FINISHED'}

class Operator_Simply_faced_Add(bpy.types.Operator):

    bl_idname = "simplyrig_faced.add_rig"
    bl_label = "simply faced Rig"
    bl_description = "Generates simply faced rig"
    bl_options = {'REGISTER', 'UNDO',}


    @classmethod
    def poll(cls, context):            
        return bpy.context.scene != None

    def import_simply(self, context):
        CURRENT_DIR = os.path.dirname(__file__)
        filepath =  os.path.join(CURRENT_DIR, "simply_rig_biped_faced.blend")
        scene = bpy.context.scene

        with bpy.data.libraries.load(filepath, link=False) as (data_from, data_to):
            data_to.collections = ["rig"]
            
        for collection in data_to.collections:
            scene.collection.children.link(collection)


    def execute(self, context):
        self.import_simply(context)
        return{'FINISHED'}
    
class OBJECT_MT_Addmenusimplyrig(bpy.types.Menu):
    bl_label = "Select"
    bl_idname = "OBJECT_MT_Addmenusimplyrig"

    def draw(self, context):
        layout = self.layout
        layout.operator("simplyrig.add_rig", text="Biped", icon='ARMATURE_DATA')
        layout.operator("simplyrig_faced.add_rig", text="Biped faced", icon='ARMATURE_DATA')
        
class INFO_MT_Simply_add_rig(bpy.types.Menu):
    
    bl_idname = "INFO_MT_Simply_add_rig"
    bl_label = "Simply add rigs"

    def draw(self, context):
        layout = self.layout
        layout.operator_context = 'INVOKE_REGION_WIN'
#        layout.operator("simplyrig.add_rig", text="SimplyRig Biped", icon='ARMATURE_DATA')
#        layout.operator("simplyrig_faced.add_rig", text="SimplyRig Biped", icon='ARMATURE_DATA')
        layout.menu(OBJECT_MT_Addmenusimplyrig.bl_idname,text="Simply rig", icon="MOD_ARMATURE")
        
def simply_add_menu_func(self, context):
#    self.layout.operator("simplyrig.add_rig", text="SimplyRig Biped", icon='ARMATURE_DATA')
#    self.layout.operator("simplyrig_faced.add_rig", text="SimplyRig Biped Faced", icon='ARMATURE_DATA')
    self.layout.menu(OBJECT_MT_Addmenusimplyrig.bl_idname,text="Simply rig", icon="MOD_ARMATURE")
    
def register_Add_Rig():

    bpy.types.VIEW3D_MT_armature_add.append(simply_add_menu_func)
    bpy.utils.register_class(Operator_Simply_Add)
    bpy.utils.register_class(Operator_Simply_faced_Add)
    bpy.utils.register_class(OBJECT_MT_Addmenusimplyrig)

    
def unregister_Add_Rig():

    bpy.types.VIEW3D_MT_armature_add.remove(simply_add_menu_func) 
    bpy.utils.unregister_class(Operator_Simply_Add)
    bpy.utils.unregister_class(Operator_Simply_faced_Add)
    bpy.utils.unregister_class(OBJECT_MT_Addmenusimplyrig)

if __name__ == "__main__":
    register_Add_Rig()